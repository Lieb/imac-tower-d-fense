/*! \file graphics.h
    \brief Le coeur du jeu, tous ce qui concerne la gestion du graphisme.
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-05-20
*/

#ifndef _ITD__GRAPHICS_H___
#define _ITD__GRAPHICS_H___

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL_image.h>
#include "../include/modules/itd.h"
#include "../include/modules/map.h"
#include "../lib/GM/include/list.h"
#include "../include/modules/player.h"

/*! \fn void drawAll(Itd itd, Map *map, List *listM, List *listT, Player *player)
    \brief Permet d'initialisé le dessin OpenGL.
        Définit tout les dessins necessaire.
    \param itd
    \param map
    \param listM
    \param listT
    \param player
*/
void drawAll(Itd itd, Map *map, List *listM, List *listT, Player *player);

/*! \fn GLuint createTexture(GLuint texture, SDL_Surface *image)
    \brief Permet de créer une texture quelconque.
        Définit tout les paramètres necessaire à une texture.
    \param texture
    \param prend l'image à plaquer sur la texture.
*/
GLuint createTexture(GLuint texture, SDL_Surface *image);

/*! \fn Uint32 getPixel(SDL_Surface *imgMap, int x, int y)
    \brief Permet de se positionner sur un pixel pour voir son contenu.
    \param SDL_Surface *imgMap prend la surface de la map
    \param int x coordonnée x du pixel
    \param int y coordonnée y du pixel
    \result On obtient un pixel en position (x,y)
    \return Uint32 retourne un entier qui décrit un pixel (rouge, vert, bleu et alpha).
*/
Uint32 getPixel(SDL_Surface *imgMap, int x, int y);

/*! \fn void definePixel(SDL_Surface *imgMap, int x, int y, Uint32 pixel)
    \brief Permet de valider le changement de couleur sur le pixel qu'on a selectionné
    \param imgMap prend la surface de la map
    \param x coordonnée x du pixel
    \param y coordonnée y du pixel
    \param pixel pixel qu'on a modifié
    \result On change le pixel par celui modifié
*/
void definePixel(SDL_Surface *imgMap, int x, int y, Uint32 pixel);

/*! \fn GLenum getSurfaceFormat(SDL_Surface* img)
    \brief Permet de recupérer le format de la SDL_Surface.
    \param img prend une surface
    \result permet de connaitre le format de la surface passé.
*/
GLenum getSurfaceFormat(SDL_Surface* img);

#endif