/*! \file control.h
    \brief Le coeur du jeu, tous ce qui concerne la gestion des évènements.
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-05-21
*/

#ifndef _ITD__CONTROL_H___
#define _ITD__CONTROL_H___

#include  <AntTweakBar.h>
#include "../include/modules/player.h"
#include "../include/modules/map.h"
#include "../lib/GM/include/list.h"
#include "../include/modules/interface.h"

/*! \fn void event(SDL_Event e, int* loop, TwBar *menu, TwBar **twTower, TwBar **twMonster, Player *player, Map *map, List *listM, List *listT, Tools *tools)
    \brief Définit tous les contrôles du jeu et les actions qui y sont assignées.
        Définit toutes les actions necessaire.
    \param e récupère l'evènement SDL.
    \param loop récupère la variable de la loop principale.
    \param menu
    \param twTower
    \param twMonster
    \param player
    \param map
    \param listM liste de monstre
    \param listT liste de Tours
    \param tools récupère les outils necessaire à AntTweakBar pour faire le lien entre les events SDL, les events ATB et les fonctions CB ATB.
*/
void event(SDL_Event e, int* loop, TwBar *menu, TwBar **twTower, TwBar **twMonster, Player *player, Map *map, List *listM, List *listT, Tools *tools);

#endif