/*! \file square.h
    \brief structure d'un carré
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-05-02
*/

#ifndef _ITD__SQUARE_H___
#define _ITD__SQUARE_H___

#include "../include/modules/coordinates.h"
#include "../lib/GM/include/geometry/shapeGL.h"
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

/*! \struct Square square.h "include/modules/square.h"
 *  \brief Structure d'un carré
*/
typedef struct square{
    CoordOpenGL coordGL;    /*!< CoordOpenGL coordGL coordonées du carré en OpenGL*/
    int nbFragSquare;       /*!< int nbFragSquare nombre de fragment par carré*/
    float lenghtSquare;     /*!< float lenghtSquare taille d'un coté du carré en OpenGL*/
    int nbFragConstruct;    /*!< int nbFragConstruct nombre de fragments constructibles*/
    int availability;       /*!< int availability BOOL qui indique si le carré est une zone constructible ou pas */
    int initSquare;         /*!< int initSquare utilisé pour le carré qu'on dessine*/
}Square;

/*! \fn Square createSquare(float xFirstFrag, float yFirstFrag, int nbFragSquare, float lenghtSquare)
    \brief crée un carré.
    \param xFirstFrag coordonnée x du premier fragment du carré
    \param yFirstFrag coordonnée y du premier fragment du carré
    \param nbFragSquare nombre de fragments dans le carré
    \param lenghtSquare taille du carré en OpenGL
    \result renvoie le carré créé.
    \return Square
*/
Square createSquare(float xFirstFrag, float yFirstFrag, int nbFragSquare, float lenghtSquare);

/*! \fn void fulfillSquareConstruct(Square *square, int nbSquare)
    \brief parcourt tous les carrés pour initialiser leur variable availibility à 0 ou 1.
    \param square tableau des carrés de la map
    \param nbSquare nombre de carrés au total dans la map
    \result associe un carré a une zone constructible.
*/
void fulfillSquareConstruct(Square *square, int nbSquare);

/*! \fn Square findSquare(CoordOpenGL coord, Square *squareTab, int nbSquare)
    \brief parcourt tous les carrés pour trouver celui dont on a passé les coordonnées en parametre.
    \param coord coordonnées du carré qu'on recherche/sur lequel on a cliqué
    \param squareTab tableau des carrés qu'on parcourt
    \param nbSquare nombre de carrés dans la map
    \result on trouve le carré qu'on voulait
    \return Square
*/
Square findSquare(CoordOpenGL coord, Square *squareTab, int nbSquare);

/*! \fn void drawSquare(Square square)
    \brief prend un carré et le dessine au bon endroit avec une couleur rouge si availability = 0 ou vert si availability = 1
    \param square carré de la map qu'on souhaite dessiner
    \result dessine un carré
*/
void drawSquare(Square square);

/*! \fn void drawSquareGrid(Square square)
    \brief dessine le contour de chaque carré
    \param square carré de la map qu'on souhaite dessiner
    \result dessine tous les carrés donc la grille de la map
*/
void drawSquareGrid(Square square);

#endif