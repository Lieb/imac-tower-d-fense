/*! \file interface.h
    \brief interface AntTweakBar du jeu
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-04-08
*/

#ifndef _ITD__INTERFACE_H___
#define _ITD__INTERFACE_H___

#include  <AntTweakBar.h>
#include "../include/modules/player.h"
#include "../include/modules/map.h"
#include "../lib/GM/include/list.h"
#include "../include/modules/tower.h"
#include "../include/modules/monster.h"
#include <GL/gl.h>
#include <GL/glu.h>

/*! \struct tools interface.h "include/modules/interface.h"
    \brief Structure des outils necessaire aux fonctions callback de l'interface.
*/
typedef struct tools{
    GLuint *textureT;       /*!< GLuint *textureT tableau des textures de tours */
    GLuint *textureM;       /*!< GLuint *textureM tableau des textures des monstres */
    List *listTower;        /*!< List *listTower liste des tours */
    List *listMonster;      /*!< List *listMonster liste des monstres */
    Player *player;         /*!< Player *player adresse du joueur */
    Map *map;               /*!< Map *map adresse de la map */
    TwBar *menu;            /*!< TwBar *menu menu antTweakBar */
    TwBar *monster;         /*!< TwBar *monster menu des monstres */
    TwBar **tower;          /*!< TwBar **tower menu des tours */
    int priceRocket;        /*!< int priceRocket prix des tours de type rocket */
    int priceHybrid;        /*!< int priceHybrid prix des tours de type hybrid */
    int priceLaser;         /*!< int priceLaser prix des tours de type laser */
    int priceMachineGun;    /*!< int priceMachineGun prix des tours de type machinegun */
    int *loop;
    int *replay;
}Tools;

/*! \struct menuITD interface.h "include/modules/interface.h"
    \brief Structure du menuITD necessaire aux fonctions callback de l'interface.
*/
typedef struct menu_itd{
    List itds;       /*!< List itds list des choix des maps itds. */
    char *itd;       /*!< char *itd map itd choisi dans le menu. */
}MenuITD;

/*! \fn Tools* initTools(GLuint *textureT, GLuint *textureM, List *listTower, List *listMonster, Player *player, Map *map, TwBar *menu, TwBar *monster, TwBar **tower)
    \brief Permet d'initialisé les outils et ressources necessaire au rafraichissement des variables ATB.
    \param textureT Texture d'une tower.
    \param textureM Texture d'un monster.
    \param listTower récupère la liste de tour.
    \param listMonster récupère la listre de monstre.
    \param player passe l'objet player.
    \param map passe l'objet map.
    \param menu récupère le menu menu ATB.
    \param monster récupère le menu monster ATB.
    \param tower récupère le menu tower ATB.
    \result Créer et initialise la structure tools.
*/
Tools* initTools(GLuint *textureT, GLuint *textureM, List *listTower, List *listMonster, Player *player, Map *map, TwBar *menu, TwBar *monster, TwBar **tower, int *loop, int *replay);

/*! \fn MenuITD* initMenuITD(List itds)
    \brief Permet d'initialisé le menuITD.
    \param itds liste des map itd contenu dans le dossier data/
    \result Créer et initialise la structure menuITD.
*/
MenuITD* initMenuITD(List itds);

/*! \fn void initAntTwBar(unsigned int width, unsigned int height)
    \brief Permet d'initialisé le contexte AntTweakBar
    \param width largeur de la fenetre SDL
    \param height longueur de la fenetre SDL
    \result on donne la taille de la fenetre à AntTweakBar afin qu'il ne l'a dépasse pas
*/
void initAntTwBar(unsigned int width, unsigned int height);

/*! \fn void createTwBar(TwBar **bar, char *name)
    \brief Crée une TwBar et lui donne un nom
    \param bar pointeur sur la bar
    \param name nom de la bar que l'on veut lui donner
    \result on donne crée une Twbar et on la nomme
*/
void createTwBar(TwBar **bar, char *name);

/*! \fn void positionTwBar(unsigned int w_width, TwBar* bar)
    \brief place une TwBar menu
    \param w_width coordonnée des abscisses
    \param bar TwBar sur laquelle on applique la position
    \result TwBar menu positionnée
*/
void positionTwBar(unsigned int w_width, TwBar* bar);

/*! \fn void positionPopUpTower(Tower *tower, TwBar* bar, int w_height, int w_width)
    \brief place une TwBar Tower
    \param bar TwBar sur laquelle on applique la position
    \param tower adresse de la tour
    \param w_height permet de convertir les coordonnées OpenGL en SDL
    \param w_width permet de convertir les coordonnées OpenGL en SDL
    \result TwBar tower positionnée
*/
void positionPopUpTower(Tower *tower, TwBar* bar, int w_height, int w_width);

/*! \fn void positionPopUpMonster(Monster *monster, TwBar* bar, int w_height, int w_width)
    \brief place une TwBar Tower
    \param bar TwBar sur laquelle on applique la position
    \param monster adresse du monstre
    \param w_height permet de convertir les coordonnées OpenGL en SDL
    \param w_width permet de convertir les coordonnées OpenGL en SDL
    \result TwBar monster positionnée
*/
void positionPopUpMonster(Monster *monster, TwBar* bar, int w_height, int w_width);

/*! \fn void sizeTwBar(unsigned int w_width, unsigned int w_height, TwBar *bar)
    \brief donne une taille a une TwBar
    \param w_width largeur de la TwBar
    \param w_height hauteur de la TwBar
    \param bar bar dont on veut modifier la taille
    \result TwBar redimensionnée
*/
void sizeTwBar(unsigned int w_width, unsigned int w_height, TwBar *bar);

/*! \fn void TW_CALL setSelectTower(const void *value, void *clientData)
    \brief Fonction callBack setSelectTower.
        Cette fonction permet d'acheter une tour et de la placer dans la liste de tour en definissant le mode
        isSet de la tour à 0 et ainsi gérer le drag'n'drop.
    \param value valeur choisi grâce au twAddVarCB de l'interface.
    \param clientData objet à modifier.
    \result Permet d'acheter une tour.
*/
void TW_CALL setSelectTower(const void *value, void *clientData);

/*! \fn void TW_CALL getSelectTower(void *value, void *clientData)
    \brief Fonction callBack getSelectTower.
        Cette fonction permet de mettre à jour la valeur dans notre interface ATB de notre liste.
    \param value valeur choisi grâce au twAddVarCB de l'interface.
    \param clientData objet à modifier.
    \result Permet de mettre à jour la tour acheté.
*/
void TW_CALL getSelectTower(void *value, void *clientData);

/*! \fn void TW_CALL setChooseITDCB(const void *value, void *clientData)
    \brief Fonction callBack setChooseITDCB.
        Cette fonction permet de choisir le fichier itd dans le menu d'accueil du jeu.
    \param value valeur choisi grâce au twAddVarCB de l'interface.
    \param clientData objet à modifier.
    \result Permet de selectionner un fichier itd dans la liste des fichiers itds disponible.
*/
void TW_CALL setChooseITDCB(const void *value, void *clientData);

/*! \fn void TW_CALL getChooseITDCB(void *value, void *clientData)
    \brief Fonction callBack getChooseITDCB.
        Cette fonction permet de mettre à jour la valeur dans notre interface ATB de notre liste.
    \param value valeur choisi grâce au twAddVarCB de l'interface.
    \param clientData objet à modifier.
    \result Permet de mettre à jour le fichier itd selectionné dans la liste.
*/
void TW_CALL getChooseITDCB(void *value, void *clientData);

/*! \fn void TW_CALL nextWaveCB(void *clientData)
    \brief Fonction callBack nextWaveCB.
    \param clientData objet à modifier.
    \result fonction appellé par le bouton.
*/
void TW_CALL nextWaveCB(void *clientData);

/*! \fn void TW_CALL playCB(void *clientData)
    \brief Fonction callBack playCB.
    \param clientData objet à modifier.
    \result fonction appellée par le bouton 'play' permettant de commencer le jeu arpès avoir selectionné un .itd l'application.
*/
void TW_CALL playCB(void *clientData);

/*! \fn void TW_CALL replayCB(void *clientData)
    \brief Fonction callBack replayCB.
    \param clientData objet à modifier.
    \result fonction appellée par le bouton 'replay' permettant de rejouer après avoir fini le jeu.
*/
void TW_CALL replayCB(void *clientData);

/*! \fn void TW_CALL ruleCB(void *clientData)
    \brief Fonction callBack ruleCB.
    \param clientData objet à modifier.
    \result fonction appellée par le bouton 'rules' permettant d'afficher les règles dans le menu d'accueil de l'application.
*/
void TW_CALL ruleCB(void *clientData);

/*! \fn void TW_CALL deleteTowerCB(void *clientData)
    \brief Fonction callBack deleteTowerCB.
    \param clientData objet à modifier.
    \result fonction appellé par le bouton.
*/
void TW_CALL deleteTowerCB(void *clientData);

/*! \fn void TW_CALL upgradeTowerCB(void *clientData)
    \brief Fonction callBack upgradeTowerCB.
    \param clientData objet à modifier.
    \result fonction appellé par le bouton.
*/
void TW_CALL upgradeTowerCB(void *clientData);

/*! \fn void TW_CALL quitCB(void *clientData)
    \brief Fonction callBack quit.
    \param clientData objet à modifier.
    \result fonction appellée par le bouton 'quit ?' permettant de quitter l'application.
*/
void TW_CALL quitCB(void *clientData);

/*! \fn void setWelcomeMenu(TwBar *bar, TwBar *ruleBar, MenuITD *menu, int *loop, int *start)
    \brief donne des variables / valeurs au menu d'accueil.
    \param bar adresse de la bar à laquelle s'applique les modifications.
    \param ruleBar adresse de la bar des regles pour créer une action CallBack dessus.
    \param menu remplit la liste et le choix de l'utilisateur.
    \param loop variable d'arret de la boucle infinie.
    \param start variable qui permet de restart le jeu.
    \result menu principal initialisé avec tous ses parametres.
*/
void setWelcomeMenu(TwBar *bar, TwBar *ruleBar, MenuITD *menu, int *loop, int *start);

/*! \fn void setLastMenu(TwBar *bar, Tools *tools)
    \brief donne des variables / valeurs au menu de de fin en WIN ou LOOSE.
    \param bar adresse de la bar à laquelle s'applique les modifications
    \param tools outils necessaire à la gestion ATB.
    \result menu de fin initialisé avec tous ses parametres.
*/
void setLastMenu(TwBar *bar, Tools *tools);

/*! \fn void setRulesMenu(TwBar *bar)
    \brief donne des variables / valeurs à la fenêtre de règles.
    \param bar adresse de la bar à laquelle s'applique les modifications.
    \result fenêtre de règles initialisé avec tous ses parametres
*/
void setRulesMenu(TwBar *bar);

/*! \fn void setMainMenu(TwBar *bar, int *loop, Tools *tools)
    \brief donne des variables / valeurs au menu principal
    \param bar adresse de la bar à laquelle s'applique les modifications
    \param loop adresse de la variable de la boucle (sert à quitter le programme)
    \param tools adresse de la structure tools comprenant tous les outils dont AntTweakBar à besoin
    \result menu principal initialisé avec tous ses parametres
*/
void setMainMenu(TwBar *bar, int *loop, Tools *tools);

/*! \fn void setTwTower(TwBar *bar, Tower *t, Tools *tools)
    \brief donne des variables / valeurs au menu tower
    \param bar adresse de la bar à laquelle s'applique les modifications
    \param t adresse de la tour selectionnée dont on veut les informations
    \param tools adresse de la structure tools comprenant tous les outils dont AntTweakBar à besoin
    \result menu tower initialisé avec tous ses parametres
*/
void setTwTower(TwBar *bar, Tower *t, Tools *tools);

/*! \fn void setTwMonster(TwBar *bar, Monster *m, Tools *tools)
    \brief donne des variables / valeurs au menu monster
    \param bar adresse de la bar à laquelle s'applique les modifications
    \param m adresse du monstre selectionné dont on veut les informations
    \param tools adresse de la structure tools comprenant tous les outils dont AntTweakBar à besoin
    \result menu monster initialisé avec tous ses parametres
*/
void setTwMonster(TwBar *bar, Monster *m, Tools *tools);

#endif