/*! \file player.h
    \brief Tous ce qui concerne le joueur.
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-05-15
*/

#ifndef _ITD__PLAYER_H___
#define _ITD__PLAYER_H___

#include "../include/modules/coordinates.h"
/*!
    \typedef Structure d'un joueurC
    \abstract Structure qui définit ce qu'est un joueurC
    \field life vie du joueur.
    \field money argent utilisable du joueur.
    \field lvl permet de définir le niveau du joueur.
 */
typedef struct parameters{
    unsigned int w_height;
    unsigned int w_width;
}Parameters;

/*!
    \typedef Structure d'un joueurC
    \abstract Structure qui définit ce qu'est un joueurC
    \field life vie du joueur.
    \field money argent utilisable du joueur.
    \field lvl permet de définir le niveau du joueur.
 */
typedef struct player{
    int life;
    int money;
    int lvl;
    Parameters param;
    CoordSDL mouseSDL;
    int constructMode;
    int gameTimer;
}Player;

/*! \fn Player* createPlayer()
    \brief Permet de créer un joueur.
    \result renvoie un joueur.
*/
Player* createPlayer();

/*! \fn initPlayer()
    \brief Permet d'initialiser les caractéristiques d'un joueur.
    \param p Joueur à initialiser.
    \result renvoie un joueur initialisé.
*/
void initPlayer(Player *p);

/*! \fn getLifePlayer()
    \brief Permet de récupérer la resistance.
    \param p Player à modifier.
    \result Récupère la valeur voulu.
*/
int getLifePlayer(Player *p);

/*! \fn setLifePlayer()
    \brief Permet de récupérer la resistance.
    \param p Player à modifier.
    \param v à modifier.
    \result Récupère la valeur voulu.
*/
void setLifePlayer(Player *p, int v);

/*! \fn getMoneyPlayer()
    \brief Permet de récupérer la resistance.
    \param p Player à modifier.
    \result Récupère la valeur voulu.
*/
int getMoneyPlayer(Player *p);

/*! \fn setMoneyPlayer()
    \brief Permet de récupérer la resistance.
    \param p Player à modifier.
    \param v à modifier.
    \result Récupère la valeur voulu.
*/
void setMoneyPlayer(Player *p, int v);

/*! \fn int getLvlPlayer(Player *p)
    \brief Permet de récupérer la resistance.
    \param p Player à modifier.
    \result Récupère la valeur voulu.
*/
int getLvlPlayer(Player *p);

/*! \fn void setLvlPlayer(Player *p, int v)
    \brief Permet de modifier le level du player.
    \param p Player à modifier.
    \param v à modifier.
    \result modifie la valeur voulu.
*/
void setLvlPlayer(Player *p, int v);

/*! \fn void changeConstructMode(Player *p)
    \brief Permet de changer le mode de construction.
    \param p Player à modifier.
    \result change si le joueur est en mode construct ou non.
*/
void changeConstructMode(Player *p);

#endif