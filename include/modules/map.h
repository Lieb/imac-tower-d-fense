/*! \file map.h
    \brief Tous ce qui concerne les maps.
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-05-18
*/

#ifndef _ITD__MAP_H___
#define _ITD__MAP_H___

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "../include/modules/itd.h"
#include "../include/modules/coordinates.h"
#include "../include/modules/square.h"

/*! \struct map map.h "include/modules/map.h"
 *  \brief Structure d'une map
*/
typedef struct map{
	char *path;             /*!< char *path chemin de l'image de la map*/
    GLuint *texture;        /*!< GLuint *texture texture de la map*/
    int heightSDL;          /*!< int heightSDL hauteur de la map en SDL (pixel)*/
    int widthSDL;           /*!< int widthSDL largeur de la map en SDL (pixel)*/
    float heightGL;         /*!< float heightGL hauteur de la map en OpenGL*/
    float widthGL;          /*!< float widthGL largeur de la map en OpenGL*/
    CoordOpenGL mouseGL;    /*!< CordOpenGL mouseGL dernière position de la souris sur la map en OpenGL*/
    Square squareDrew;      /*!< Square squareDrew carré a dessiné (en mode construct ou debug)*/
	int nbNodes;            /*!< int nbNodes nombre de noeuds pour tracer le chemin de la map*/
	CoordOpenGL *nodesGL;   /*!< CoordOpenGL *nodesGL tableau des coordonnées des nodes en OpenGL*/
    int nbSquare;           /*!< int nbSquare nombre de carrés dans la map*/
    Square *squareTab;      /*!< Square *squareTab tableau des carrés de la map*/
    int wave;               /*!< int  wave permet de connaitre le nombre de monstres déjà envoyés dans la vague*/
    int delayMonster;       /*!< int delayMonster délai entre l'apparition de chaque monstre*/
    int timerWave;          /*!< int timerWave décompte le délai d'apparition entre chaque monstres*/
    int debug;              /*!< int debug mode debug activé ou non (BOOL)*/
    int pause;              /*!< int pause mode pause activée ou non (BOOL)*/
}Map;

/*! \fn createMap()
    \brief Permet de créer une map.
    \result renvoie l'adresse d'une map.
*/
 Map* createMap();

/*! \fn Map* initMap(Map *m, char *pathMap, GLuint *textureMap, int nbN, CoordOpenGL *nodes, int nbS, Square *squareT, unsigned int width, unsigned int height, float widthGL, float heightGL)
    \brief Permet d'initialiser les caractéristiques d'une map.
    \param m adresse de la Map à initialiser.
    \param pathMap chemin de la map à charger
    \param textureMap texture de la map
    \param nbN nombre de nodes
    \param nodes tableau des nodes
    \param nbS nombre de carrés
    \param squareT tableau des carrés
    \param width largeur de la fenetre
    \param height hauteur de la fenetre
    \param widthGL largeur de la fenetre en OpenGL
    \param heigthGL hauteur de la fenetre en OpenGL
    \result renvoie l'adresse d'une map.
*/
 Map* initMap(Map *m, char *pathMap, GLuint *textureMap, int nbN, CoordOpenGL *nodes, int nbS, Square *squareT, unsigned int width, unsigned int height, float widthGL, float heightGL);

/*! \fn void drawMap(GLuint *textureMap)
    \brief Permet de dessiner la map chargée dans le quad principal.
        Définit le quad de la map et la texture appliqué dessus.
    \param textureMap texture de la map à dessiner
    \result dessine la map
*/
void drawMap(GLuint *textureMap);

/*! \fn void drawRoad(CoordOpenGL *nodesGL, Itd itd)
    \brief Permet de dessiner le chemin emprunté par les monstres
    \param coordGL tableau de coordonnées du chemin
    \param itd prend la structure du fichier itd contenant toutes les infos de celui-ci
    \result dessine des segments joints aux noeuds du chemin
*/
void drawRoad(CoordOpenGL *nodesGL, Itd itd);

/*! \fn void drawNode(CoordOpenGL *nodesGL, Itd itd)
    \brief Permet de dessiner les noeuds du chemin emprunté par les monstres
    \param coordGL tableau de coordonnées du chemin
    \param itd prend la structure du fichier itd contenant toutes les infos de celui-ci
    \result dessine les noeuds du chemin et les colore
*/
void drawNode(CoordOpenGL *nodesGL, Itd itd);

/*! \fn void drawPause(Map *map)
    \brief Permet de dessiner les noeuds du chemin emprunté par les monstres
    \param map Passe la map pour dessiner dessus.
    \result dessine un quad sur tout l'écran pour faire la pause.
*/
void drawPause(Map *map);

/*! \fn void colorMap(SDL_Surface *imgMap, Itd itd)
    \brief Permet de changer la couleur de la zone constructible de la map
    \param imgMap prend la surface de la map
    \param itd prend la structure du fichier itd contenant les infos de couleurs
    \result change la couleur des zones constructibles pixel par pixel
*/
void colorMap(SDL_Surface *imgMap, Itd itd);

/*! \fn int sizeConstructMap(SDL_Surface *imgMap)
    \brief Permet de récupérer la taille du tableau de zone constructible de la map.
    \param imgMap prend la surface de la map.
    \result retourne la taille du tableau construct.
*/
int sizeConstructMap(SDL_Surface *imgMap);

/*! \fn void setConstructMap(SDL_Surface *imgMap, Square *square, int w_height, int w_width, int nbSquare)
    \brief Permet de construire le tableau de zone constructible de la map.
    \param imgMap prend la surface de la map.
    \param constructSDL prend le tableau de coordonnées constructible à remplir.
    \result remplit le tableau de zone constructible.
*/
void setConstructMap(SDL_Surface *imgMap, Square *square, int w_height, int w_width, int nbSquare);

/*! \fn void changeAvailability(Square squareSelected, Map *map)
    \brief Permet de changer tout un carré en zone non construcible apres la pose d'une tour dessus
    \param squareSelected carré selectionné dont on va changer availability
    \param map adresse de la map
    \result change a non constructible le squareSelected.
*/
void changeAvailability(Square squareSelected, Map *map);

#endif