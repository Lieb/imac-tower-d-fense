/*! \file coordinates.h
    \brief structure d'une coordonnée (x,y) SDL et OpenGL
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-04-22
*/

#ifndef _ITD__COORD_H___
#define _ITD__COORD_H___

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*! \def abscissaLength
    \brief largeur de l'axe des abscisses en OpenGL
*/
#define abscissaLength 4

/*! \def abscissaRefocusing
    \brief abscisse permettant de recadrer les éléments d'une conversion
*/
#define abscissaRefocusing 2

/*! \def orderedLength
    \brief hauteur de l'axe des ordonnées en OpenGL
*/
#define orderedLength 3

/*! \def orderedRefocusing
    \brief ordonnée permettant de recadrer les éléments d'une conversion
*/
#define orderedRefocusing (-1.5)

/*! \enum Axes coordinates.h "include/modules/coordinates.h"
 *  \brief Enum des axes 
    \param abscissa
    \param ordered
*/
typedef enum axes{
    ABSCISSA,
    ORDERED
}Axes;

/*! \struct CoordSDL coordinates.h "include/modules/coordinates.h"
 *  \brief Structure d'une coordonnée SDL(x,y)
*/
typedef struct coordSDL{
    int x;	/*!< int x coordonnée des abscisses*/
    int y;	/*!< int y coordonnée des ordonnées*/
}CoordSDL;

/*! \struct CoordOpenGL coordinates.h "include/modules/coordinates.h"
 *  \brief Structure d'une coordonnée OpenGL(x,y)
*/
typedef struct coordOpenGL{
    float x;	/*!< float x coordonnée des abscisses*/
    float y;	/*!< float y coordonnée des ordonnées*/
}CoordOpenGL;

/*! \struct CoordSquare coordinates.h "include/modules/coordinates.h"
 *  \brief Structure d'une coordonnée d'un Carré(x,y)
*/
typedef struct coordsquare{
    int x;  /*!< int x coordonnée des abscisses*/
    int y;  /*!< int y coordonnée des ordonnées*/
}CoordSquare;

/*! \fn CoordOpenGL coordSDLtoCoordOpenGL(CoordSDL coord, int w_height, int w_width)
    \brief permet de convertir les coordonnées SDL en coordonnées OpenGL
    \param coord coordonnées SDL
    \param w_height hauteur de la fenetre SDL
    \param w_width largeur de la fenetre SDL
    \result convertir les coordonnées SDL en OpenGL avec l'axe ortho
    \return retourne les coordonnées OpenGL converties
*/
CoordOpenGL coordSDLtoCoordOpenGL(CoordSDL coord, int w_height, int w_width);

/*! \fn CoordSDL coordOpenGLtoCoordSDL(CoordOpenGL coord, int w_height, int w_width)
    \brief permet de convertir les coordonnées OpenGL en coordonnées SDL
    \param coord coordonnées OpenGL
    \param w_height hauteur de la fenetre SDL
    \param w_width largeur de la fenetre SDL
    \result convertir les coordonnées OpenGL avec l'axe ortho en coordonnées SDL
    \return retourne les coordonnées SDL converties
*/
CoordSDL coordOpenGLtoCoordSDL(CoordOpenGL coord, int w_height, int w_width);

/*! \fn CoordSDL changeCoordMark(CoordSDL coord, int w_height, int w_width, int w_image, int h_image)
    \brief permet de convertir les coordonnées du fichier itd en coordonnées sur la map affichée
    \param coord coordonnées SDL
    \param w_height hauteur de la fenetre SDL
    \param w_width largeur de la fenetre SDL
    \param w_image largeur de l'image
    \param h_image hauteur de l'image
    \result convertir les coordonnées SDL dans le repere de la map chargée
    \return retourne les coordonnées SDL converties
*/
CoordSDL changeCoordMark(CoordSDL coord, int w_height, int w_width, int w_image, int h_image);

/*! \fn float SDLtoOpenGL(int x, int scaleSDL, Axes axis)
    \brief permet de convertir les int SDL en float OpenGL.
    \param x valeur à convertir en OpenGL.
    \param scaleSDL échelle SDL qui permet de faire la conversion
    \param axis axe par rapport à lequel on converti
    \return retourne la valeur en float OpenGL convertie.
*/
float SDLtoOpenGL(int x, int scaleSDL, Axes axis);

/*! \fn int OpenGLtoSDL(float x, float scaleGL, Axes axis)
    \brief permet de convertir les float OpenGL en int SDL.
    \param x valeur à convertir en SDL.
    \param float scaleGL échelle OpenGL qui permet de faire la conversion
    \param axis axe par rapport auquel on converti
    \return retourne la valeur en int SDL convertie.
*/
int OpenGLtoSDL(float x, float scaleGL, Axes axis);

/*! \fn CoordOpenGL coordSDLmaptoCoordOpenGLmap(CoordSDL coord, int w_height, int w_width, float heightGL, float widthGL)
    \brief permet de convertir des coordonnées SDL en coordonnées OpenGL dans le repere de la map
    \param coord coordonnées SDL à convertir en OpenGL
    \param w_height hauteur de la fenetre en SDL
    \param w_width largeur de la fenetre en SDL
    \param heightGL hauteur en OpenGL
    \param widthGL largeur en OpenGL
    \return retourne la valeur en int SDL convertie.
*/
CoordOpenGL coordSDLmaptoCoordOpenGLmap(CoordSDL coord, int w_height, int w_width, float heightGL, float widthGL);

/*! \fn CoordSDL coordOpenGLmaptoCoordSDLmap(CoordOpenGL coord, int w_height, int w_width, float heightGL, float widthGL)
    \brief permet de convertir des coordonnées OpenGL en coordonnées SDL dans le repere de la map
    \param coord coordonnées OpenGL à convertir en SDL
    \param w_height hauteur de la fenetre en SDL
    \param w_width largeur de la fenetre en SDL
    \param heightGL hauteur en OpenGL
    \param widthGL largeur en OpenGL
    \return retourne la valeur en CoordSDL convertie.
*/
CoordSDL coordOpenGLmaptoCoordSDLmap(CoordOpenGL coord, int w_height, int w_width, float heightGL, float widthGL);

/*! \fn CoordOpenGL coordSquaretoCoordOpenGL(CoordSquare coord, int w_height, int w_width)
    \brief permet de convertir des coordonnées Square en coordonnées OpenGL
    \param coord coordonnées Square à convertir en OpenGL
    \param w_height hauteur de la fenetre en SDL
    \param w_width largeur de la fenetre en SDL
    \return retourne la valeur en CoordOpenGL convertie.
*/
CoordOpenGL coordSquaretoCoordOpenGL(CoordSquare coord, int w_height, int w_width);

#endif