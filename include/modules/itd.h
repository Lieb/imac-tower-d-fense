/*! \file itd.h
    \brief La gestion de tous ce qui concerne la gestion du fichier .itd
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-04-22
*/

#ifndef _ITD__ITD_H___
#define _ITD__ITD_H___

#include "colorRGB.h"
#include "coordinates.h"

/*! \struct Itd itd.h "include/modules/itd.h"
 *  \brief Structure d'un fichier itd.
 *
 *  Cette structure definit ce qu'est un fichier itd.
 */
typedef struct itd{
	char* carte;       /*!< char* carte definit le chemin de notre fichier carte en ppm*/
	RGB chemin;        /*!< RGB chemin */
	RGB color_node;    /*!< RGB color_node */
	RGB construct;     /*!< RGB construct */    
	RGB in;            /*!< RGB in*/
	RGB out;           /*!<RGB out */
    int nbNodes;       /*!<int nbNodes */
    CoordSDL *nodes;   /*!< CoordSDL *nodes */
}Itd;

/*! \fn Itd checkItd(char* fileITD)
 *  \brief permet de verifier si le fichier .itd est valide et de le sauvegarder dans une structure itd
 *  \param fileITD char* fileITD prend en parametre le fichier itd passé en argument
 *  \result struture ITD initialisé avec les bon parametres
    \return retourne la variable de structure Itd complétée
 */
Itd checkItd(char* fileITD);

/*! \fn char saveColor(char temp, RGB *object, FILE* fdat)
    \brief permet de d'enregistrer une couleur RGB d'un objet quelconque
    \param object RGB* object l'objet dont on veut sauver les couleurs
    \param temp char temp on récupère la derniere valeur parcourue par fread
    \param fdat FILE* fdat fichier que l'on parcourt
    \result sauvegarde la couleur et renvoie le char temp qui permet de continuer la ou on c'était arrêté
    \return retourne le char temp
 */
char saveColor(char temp, RGB *object, FILE* fdat);

/*! \fn char tempName(char temp, char *temp_nom, FILE* fdat)
    \brief enregistre temporairement le nom du champs renseigné
    \param temp_nom char* temp_nom tableau qui contiendra le nom du champs
    \param temp char temp on récupère la derniere valeur parcourue par fread
    \param fdat FILE* fdat fichier que l'on parcourt
    \result sauvegarde le nom et renvoie le char temp qui permet de continuer la ou on c'était arrêté
    \return retourne le char temp
 */
char tempName(char temp, char *temp_nom, FILE* fdat);

/*! \fn int saveNode(char *temp, CoordSDL *node, FILE* fdat, int cmpt)
    \brief enregistre les coordonnées des noeuds du chemin
    \param node CoordSDL* node tableau de coordonnées des noeuds
    \param temp char temp on récupère la derniere valeur parcourue par fread
    \param fdat FILE* fdat fichier que l'on parcourt
    \param int cmpt compteur de ligne de node
    \result sauvegarde les coordonnées et renvoie le nombre de lignes de node
    \return retourne le nombre de lignes de node
 */
int saveNode(char *temp, CoordSDL *node, FILE* fdat);

/*! \fn char jumpBlank(char temp, FILE* fdat)
    \brief saute les espaces, commentaires et sauts de lignes
    \param temp char temp on récupère la derniere valeur parcourue par fread
    \param fdat FILE* fdat fichier que l'on parcourt
    \result saute les espaces inutiles et renvoie le char temp qui permet de continuer la ou on c'était arrêté
    \return retourne le char temp
 */
char jumpBlank(char temp, FILE* fdat);

#endif