/*! \file colorRGB.h
    \brief structure d'une couleur en RGB
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-04-16
*/

#ifndef _ITD__COLORRGB_H___
#define _ITD__COLORRGB_H___

/*! \struct RGB colorRGB.h "include/modules/colorRGB.h"
 *  \brief Structure d'une couleur RGB.
 */

typedef struct rgb{
	unsigned char r;	/*!< unsigned char r definit la composante rouge*/
	unsigned char g;	/*!< unsigned char g definit la composante verte*/
	unsigned char b;	/*!< unsigned char b definit la composante bleue*/
}RGB;

#endif