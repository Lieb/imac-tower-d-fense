/*! \file sprite.h
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \header Header de sprite.c
        La gestion de tous ce qui concerne les sprite du jeu
    \updated 2013-05-05
 */

#ifndef ITD_SPRITE___
#define ITD_SPRITE___

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "../include/modules/coordinates.h"

/*! \enum State
    \brief Définit le statut du sprite.
        Cet Enum permet au sprite de savoir où est ce qu'il en est en état. Par exmple si un monste est
        en état 'DOWN' alors le sprite chargera l'état DOWN du dessin du monstre et bouclera sur son animation.
*/
typedef enum{
    DOWN,
    LEFT,
    RIGHT,
    UP,
    DIE
}State;

/*!
    \typedef Structure d'un sprite
    \abstract Structure qui définit ce qu'est un sprite
    \field x coordonnée x
*/
typedef struct sprite{
        GLuint *texture;
        State state;
        float width;
        float height;
        CoordOpenGL pos;
        // pour la gestion des animations
        // animation courante du sprite
        int anim;
        // le temps de l'animation courante
        int timeAnim;
        // nombre d'animations du sprite
        int nbAnim;
        // nombre d'états différent que le sprite peut prendre.
        int nbState;
}Sprite;

/*!
    \fn Sprite createSprite(Sprite s)
    \brief crée une animation à partir d'un sprite.
    \param s sprite à créer.
    \result on retourne l'image rafraichie.
*/
Sprite createSprite(Sprite s);

/*!
    \fn Sprite initSprite(Sprite s, GLuint *texture, float w, float h, CoordOpenGL pos, int nbA, int nbS)
    \brief initialise une animation à partir d'un sprite.
    \param sprite on prend en paramètre le sprite à animer.
    \param texture
    \param w
    \param h
    \param pos
    \param nbA
    \param nbS
    \result on retourne l'image rafraichie.
*/
Sprite initSprite(Sprite s, GLuint *texture, float w, float h, CoordOpenGL pos, int nbA, int nbS);

/*!
    \fn void updateStateSprite(Sprite *sprite, State state)
    \brief crée une animation à partir d'un sprite.
    \param Sprite sprite on prend en paramètre le sprite à animer.
    \param state état du sprite pour mettre à jour la hauteur.
    \result on retourne l'image rafraichie.
*/
void updateStateSprite(Sprite *sprite, State state);

/*!
    void updatePosSprite(Sprite *s, CoordOpenGL p)
    \brief Met à jour la position du sprite.
    \param s on prend en paramètre le sprite modifier.
    \param p coordonné qui mettent à jour.
    \result on modifie la pos du sprite.
*/
void updatePosSprite(Sprite *s, CoordOpenGL p);

/*!
    \fn drawSprite()
    \brief crée une animation à partir d'un sprite
    \param sprite sprite on prend en paramètre le sprite à animer
    \result on retourne l'image rafraichie
*/
void drawSprite(Sprite *sprite);

#endif