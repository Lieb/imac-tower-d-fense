#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include "../include/engine/control.h"
#include "../include/engine/system.h"
#include "../include/modules/square.h"
#include "../include/modules/tower.h"
#include "../include/modules/player.h"
#include "../include/modules/monster.h"

void event(SDL_Event e, int* loop, TwBar *menu, TwBar **twTower, TwBar **twMonster, Player *player, Map *map, List *listM, List *listT, Tools *tools) {
    if(loop == NULL || menu == NULL || player == NULL || map == NULL || listM == NULL || listT == NULL){
        fprintf(stderr, "event::Un des pointeurs n'est pas définit.\n");
        return;
    }
    int handled;

    while(SDL_PollEvent(&e)){
        // Send event to AntTweakBar
        handled = TwEventSDL(&e, SDL_MAJOR_VERSION, SDL_MINOR_VERSION);
        // Use SDL normal function
        if(!handled){
            if(e.type == SDL_QUIT){
                quit(loop);
                break;
            }
            switch(e.type) {
                case SDL_VIDEORESIZE:
                    resize(e.resize.w, e.resize.h, player);
                    positionTwBar(player->param.w_width, menu);
                    sizeTwBar(player->param.w_width, player->param.w_height, menu);
                    setVideoMode(player->param.w_width, player->param.w_height);
                    break;
                case SDL_MOUSEBUTTONUP:
                    if(e.button.button == SDL_BUTTON_LEFT){
                        Square testSquareTower;
                        Square testSquareMouse;
                        player->mouseSDL.x = e.button.x;
                        player->mouseSDL.y = e.button.y;
                        // Conversion des coord SDL en coord OpenGL.
                        map->mouseGL = coordSDLtoCoordOpenGL(player->mouseSDL, player->param.w_height, player->param.w_width);

                        if(*twTower){
                            TwDeleteBar(*twTower);
                            *twTower = NULL;
                        }
                        /* On verifie si on cliqué sur une tour */
                        foreachList(node, *listT){  
                            Tower *t = (Tower *)node->data;
                            if(t->isSet != 0){
                                t->select = 0;      // si une autre tour était selecttionnée je l'a déselectionne
                                testSquareTower = findSquare(t->coord, map->squareTab, map->nbSquare);
                                testSquareMouse = findSquare(map->mouseGL, map->squareTab, map->nbSquare);
                                float X = testSquareTower.coordGL.x - testSquareMouse.coordGL.x;
                                float Y = testSquareTower.coordGL.y - testSquareMouse.coordGL.y;

                                /* si le carré ou est placée ma tour correspond au carré ou le joueur a cliqué */ 
                                if((X < EPSILON && Y < EPSILON) && (X > -EPSILON && Y > -EPSILON)){
                                    t->select = 1;
                                    createTwBar(twTower, "Tower");
                                    TwDefine(" Tower help='Menu de la tour selectionnée\n' resizable=false fontresizable=false contained=true iconifiable=false size='250 210' color='230 220 140' text='dark' alpha=200 ");
                                    positionPopUpTower(t, *twTower, player->param.w_height, player->param.w_width);
                                    setTwTower(*twTower, t, tools);
                                }
                            }
                        } 

                        if(*twMonster){
                            TwDeleteBar(*twMonster);
                            *twMonster = NULL;
                        }
                        /* On verifie si on cliqué sur un monstre */
                        foreachList(node, *listM){
                            Monster *m = (Monster *)node->data;
                            m->select = 0;
                            if(checkMonster(map->mouseGL, m) == 1){
                                m->select = 1;
                                createTwBar(twMonster, "Monster");
                                TwDefine(" Monster help='Menu du monstre selectionné\n' resizable=false fontresizable=false contained=true iconifiable=false size='250 180' valueswidth=60 color='230 220 140' text='dark' alpha=200 ");
                                positionPopUpMonster(m, *twMonster, player->param.w_height, player->param.w_width);
                                setTwMonster(*twMonster, m, tools);
                            }
                        }

                        if(map->squareDrew.availability == 1){
                            lastList(node, *listT){
                                Tower *t = (Tower *)node->data;
                                if(t->isSet == 0){
                                    t->isSet = 1;
                                    changeAvailability(map->squareDrew, map);
                                    setMoneyPlayer(player, -(t->price));
                                    player->constructMode = 0;
                                }
                            }
                        }                     
                    }
                    break;
                case SDL_MOUSEMOTION:
                    player->mouseSDL.x = e.button.x;
                    player->mouseSDL.y = e.button.y;

                    map->mouseGL = coordSDLtoCoordOpenGL(player->mouseSDL, player->param.w_height, player->param.w_width);
                    map->squareDrew = findSquare(map->mouseGL, map->squareTab, map->nbSquare);

                    lastList(node, *listT){
                        Tower *t = (Tower *)node->data;
                        if(t->isSet == 0){
                            positionTower((Tower *)node->data, map->squareDrew.coordGL);
                        }
                    }
                    
                    // printf("%f %f\n", square->coordGL.x,square->coordGL.y);
                    map->squareDrew.initSquare = 1;            
                    break;
                case SDL_KEYDOWN:
                    switch(e.key.keysym.sym){
                        case SDLK_q : 
                        case SDLK_ESCAPE : 
                            quit(loop);
                            break;
                        case SDLK_p :
                            map->pause = 1;
                            break;
                        case SDLK_r :
                                fastForward();
                            break;
                        case SDLK_u :
                            if((winWave(*listM) == 1 && map->wave==10) || player->lvl == 0){
                                player->lvl++;
                                map->wave = 0;
                            }
                            break;
                        case SDLK_d :
                            if(map->debug == 0){
                                map->debug = 1;
                            }
                            else{
                                map->debug = 0;
                            }
                            break;
                        case SDLK_c :
                            setMoneyPlayer(player, 10000);
                            break;
                        default :
                            break;
                    }
                break;
                default:
                    break;
            }
        }
    }
}