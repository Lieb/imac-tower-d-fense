/*! \file main.c
    \brief fichier main du programme (point d'entrée de notre application).
    \author Angéline Guignard & Adrien Megueddem
    \copyright Imac Tour Defense
    \language C
    \updated 2013-04-08
*/

/*! \mainpage Imac_Tower_Defense by Angéline GUIGNARD & Adrien MEGUEDDEM IMAC 2015
*
* \section intro_sec Intro
*
* 	Ce jeu a été réalisé dans le cadre d'un projet étudiant à l'école d'ingénieur IMAC visant à approfondir les
*	notions d'algorithmique, de synthèse d'image, de programmation C, et d'utilisaton de SDL et OpenGL.
*	-----
*	This game was created as part of a student project at the IMAC engineering school to deepen the algorithmic concepts,
*	image synthesis, C programming, and Website Usage SDL and OpenGL.
*
* \section install_sec Installation
*
* 	Pour installer ce jeu il suffit d'avoir une distribution linux, de télécharger le projet et de lancer ". ./install.sh"
*	dans votre terminal puis une fois la compilation terminé de lancer "./bin/itd"
*	-----
*	To install this game you need is a linux distribution, download the project and run ".. / install.sh" in your terminal
*	then once the compilation finished running "./bin/itd"
*
* \section guide_sec User's Guide
*
* \subsection aim Aim :
*
*	Le but du jeu est d’empêcher des monstres d’arriver jusqu’au point de sortie du chemin en les attaquant
*	avec des tours. Sans action de votre part une vague de monstre se lance automatiquement au bout de 60 secondes.
*	Cependant si vous vous sentez l’âme d’un guerrier vous pouvez choisir de lancer la prochaine vague plus rapidement.
*	Votre objectif est de survivre à 20 vagues de monstres toutes plus fortes et plus résistantes les unes que les autres.
*	Afin de palier à cela vous aurez la possibilité de faire évoluer vos tours. Il y a deux niveaux d’amélioration
*	pour chacun des quatre types de tour.
*	-----
*	The goal is to prevent monsters from reaching the exit point of the path by attacking with towers.
*	No action on your part a wave of monster launches automatically after 60 seconds.
*	However if you feel the soul of a warrior you can choose to launch the next wave faster.
*	Your goal is to survive all 20 waves stronger and stronger as each other monsters.
*	To overcome this you will be able to upgrade your towers.
*	There are two levels of improvement for each of four types of towers.
*
* \subsection working Working :
*
*	On lance le jeu après un install.sh afin de définir des variables d’environnement pour les librairies
*	et les fichiers include que nous avons utilisé pour créer le jeu. Le jeu est compilé également à partir
*	de ce même fichier. Pour lancer le jeu on a alors deux options : lancer l’exécutable avec un argument
*	(le nom d’un des fichiers .itd sans oublier son extension), ou bien le lancer sans argument.
*	Dans le premier cas le jeu démarre directement sur la carte chargée par le fichier itd.
*	Dans le second cas, un menu d’accueil est lancé, où l’on a la possibilité de choisir parmi une liste de fichiers itd
*	celui qu’on souhaite charger. Une fois un choix sélectionné il suffit de cliquer sur play (raccourcis : touche espace). 
*	-----
*	The game is launched after running install.sh to set environment variables
*	for the libraries and include files that we used to create the game.
*	The game is also compiled from the same file.
*	To start the game you have two options: run the executable with an argument (the name of one of the files),
*	or run without argument.
*	In the first case the game starts directly on the map charged by the itd file.
*	In the second case, a home menu is launched, where we have the opportunity to choose from a list of files a file itd.
*	After selecting a choice just click on play (shortcut: press space).
*
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/modules/itd.h"
#include "../include/engine/system.h"

int main(int argc, char** argv) {
	int i;
	int playAgain = 0;
	char pathItd[60] = "data/\0";

	if(argc > 1){
		for (i=0;i<argc;i++) {
			// printf("arg %d : %s\n", i, argv[i]);
			if (strcmp(argv[i],"-h") == 0 || strcmp(argv[i],"--help") == 0) {
				printf("************************** Affichage de l'aide ********************************\n");
				printf("* Vous pouvez rentrer des arguments en ouvrant le programme :                 *\n");
				printf("*   - Le fichier itd contenu dans data ou bien le chemin de votre fichier itd.*\n");
				printf("*******************************************************************************\n");
				exit(1);
			}
			else if(strstr(argv[i],".itd") != NULL){ // si il y a bien un argument contenant .itd
				if(strstr(argv[i],"./data/") != NULL || strstr(argv[i],"data/") != NULL){
					printf("%s\n", argv[i]);
					strcpy(pathItd, argv[i]);
					printf("chemin : %s\n", pathItd);
					/* Appel ImacTowerDefense qui lance notre jeu. */
					playAgain = ImacTowerDefense(pathItd);
				}
				else{
					printf("%s\n",argv[i] );
					strcat(pathItd,argv[i]); // j'ajoute dans path argv[] a la suite
					printf("chemin : %s\n", pathItd);
					/* Appel ImacTowerDefense qui lance notre jeu. */
					playAgain = ImacTowerDefense(pathItd);
				}
			}
		}
	}
	else{
		playAgain = ImacTowerDefense(NULL);
	}
	while(playAgain){
		playAgain = ImacTowerDefense(NULL);
	}
	return EXIT_SUCCESS;	
}
