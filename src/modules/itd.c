#include <stdlib.h>
#include <stdio.h>
#include <string.h> 
#include "../include/modules/itd.h"

char jumpBlank(char temp, FILE* fdat){
    /* Saute les blancs */
    while ((temp == ' ' || temp == '\n' || temp == '#') && !feof(fdat)){
        if (temp == '#'){
            while (temp != '\n'){
                fread(&temp,1,1,fdat);
            }
        }
        fread(&temp,1,1,fdat);
    }
    return temp;
}

int saveNode(char *temp, CoordSDL *node, FILE* fdat){
    if((*temp >= '0') && (*temp <= '9') && !feof(fdat)){
        node->x = 0;
        while ((*temp >= '0') && (*temp <= '9') && !feof(fdat)){
            node->x = node->x * 10;
            node->x = node->x + (*temp - '0');
            fread(temp,1,1,fdat);
        }

        *temp = jumpBlank(*temp, fdat);

        node->y = 0;
        while ((*temp >= '0') && (*temp <= '9') && !feof(fdat)){
            node->y = node->y * 10;
            node->y = node->y + (*temp - '0');
            fread(temp,1,1,fdat);
        }
        return 1;
    }
    else{
        fprintf(stderr, "Ceci n'est pas un noeud car ce n'est pas une coordonée.\n");
        return 0;
    }
}

char tempName(char temp, char *temp_nom, FILE* fdat){
    int i = 0;
    if(temp_nom == NULL){
        fprintf(stderr, "Le tableau de nom temp n'existe pas\n");
        return temp;
    }
    if(fdat == NULL){
        fprintf(stderr, "Le fichier data n'existe pas\n");
        exit(1);
    }

    while(temp != ' '){
        if(temp == '\n'){
            fprintf(stderr, "L'une des valeurs est null dans votre fichier itd\n");
            exit(1);
        }
        temp_nom[i] = temp;
        i++;
        fread(&temp,1,1,fdat);
    }
    temp_nom[i] = '\0';
    return temp;
}

char saveColor(char temp, RGB *object, FILE* fdat){
    if(object == NULL){
        fprintf(stderr, "L'object n'existe pas\n");
        return temp;
    }
    if(fdat == NULL){
        fprintf(stderr, "La variable fichier n'existe pas\n");
        exit(1);
    }

    temp = jumpBlank(temp, fdat);

    /* Red color object */ 
    object->r = 0;
    while ((temp >= '0') && (temp <= '9')){
        object->r = object->r * 10;
        object->r = object->r + (temp - '0');
        fread(&temp,1,1,fdat);
    }

    temp = jumpBlank(temp, fdat);

    /* Green color object */ 
    object->g = 0;
    while ((temp >= '0') && (temp <= '9')){
        object->g = object->g * 10;
        object->g = object->g + (temp - '0');
        fread(&temp,1,1,fdat);
    }

    temp = jumpBlank(temp, fdat);

    /* Blue color object */ 
    object->b = 0;
    while ((temp >= '0') && (temp <= '9')){
        object->b = object->b * 10;
        object->b = object->b + (temp - '0');
        fread(&temp,1,1,fdat);
    }
    return temp;
}

Itd checkItd(char* fileITD){
	FILE* fdat;

	char format[20];
	char carte[60];
	char temp;
	char temp_nom[60];
	RGB chemin;
	RGB color_node;
	RGB construct;
	RGB in;
	RGB out;

    Itd itd;
	int i;
    int cmpt = 0;
    int parameters = 0;
    int nbNode = 0;

    printf("** Vérification fichier ITD **\n");

	fdat = fopen(fileITD, "rb");

	if(fdat == NULL){ 
		printf("L'ouverture du fichier a échoué ou n'existe pas.\n");
        exit(1);
	}

    /* Début de lecture du fichier */
	fread(&temp,1,1,fdat); 

    /* On enregistre le format */
    i = 0;
    while(temp != '\n'){
        format[i] = temp;
        fread(&temp,1,1,fdat);
        i++;
    }
    format[i] = '\0';
    printf("%s\n", format);
    /* Verification que c'est bien un fichier itd */
    if(strcmp(format,"@ITD 1") != 0){
        fprintf(stderr, "Ce fichier n'est pas un .itd correct\n");
        exit(1);
    }
    parameters++;

    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);
    printf("temp nom :: %s\n",temp_nom );

    /* On récupère le nom de la carte à charger */
    if(strcmp(temp_nom,"carte") == 0){
        i = 0;
        parameters++;
    	while(temp != '\n'){
			carte[i] = temp;
            fread(&temp,1,1,fdat);
            i++;       
        }
        carte[i] = '\0';
        printf("carte : %s\n", carte);
    }

    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);

    /* On verifie que c'est bien le champ chemin */
    if(strcmp(temp_nom,"chemin") == 0){
        parameters++;
        temp = saveColor(temp, &chemin, fdat);
        printf("chemin : %d %d %d\n", chemin.r, chemin.g, chemin.b);
    }    
    
    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);

    /* On verifie que c'est bien le champ noeud */
    if(strcmp(temp_nom,"noeud") == 0){
        parameters++;
        temp = saveColor(temp, &color_node, fdat);
        printf("noeud : %d %d %d\n", color_node.r, color_node.g, color_node.b);
    }
    
    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);

    /* On verifie que c'est bien le champ construct */
    if(strcmp(temp_nom,"construct") == 0){
        parameters++;
        temp = saveColor(temp, &construct, fdat);
        printf("construct : %d %d %d\n", construct.r, construct.g, construct.b);
    }
    
    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);

    /* On verifie que c'est bien le champ in */
    if(strcmp(temp_nom,"in") == 0){
        parameters++;
        temp = saveColor(temp, &in, fdat);
        printf("in : %d %d %d\n", in.r, in.g, in.b);
    }

    temp = jumpBlank(temp, fdat);
    temp = tempName(temp, temp_nom, fdat);
    temp = jumpBlank(temp, fdat);

    /* On verifie que c'est bien le champ out */
    if(strcmp(temp_nom,"out") == 0){
        parameters++;
        temp = saveColor(temp, &out, fdat);
        printf("out : %d %d %d\n", out.r, out.g, out.b);
    }
    
    temp = jumpBlank(temp, fdat);

    /* On recupere le nombre de noeuds du chemin puis les noeuds */
    while ((temp >= '0') && (temp <= '9')){
        nbNode = nbNode * 10;
        nbNode = nbNode + (temp - '0');
        fread(&temp,1,1,fdat);
    }
    if(nbNode > 0){     // si il existe des noeuds
        parameters++;
    }
    if(parameters != 8){
        fprintf(stderr, "Il manque des parametres dans votre fichier .itd\n");
        exit(1);
    }
    temp = jumpBlank(temp, fdat);
    printf("nb noeud : %d\n", nbNode);

    CoordSDL *nodes=(CoordSDL*)malloc(sizeof(CoordSDL)*nbNode);
    i=0;
    while(i < nbNode){
        temp = jumpBlank(temp, fdat);
        cmpt += saveNode(&temp, &nodes[i], fdat);
        fread(&temp,1,1,fdat);
        //printf("node %d x : %d y : %d\n", i, nodes[i].x, nodes[i].y);
        i++;
    }
    if(cmpt != nbNode){
        fprintf(stderr, "Il manque des coordonnées de noeuds\n");
        exit(1);
    }
    fclose(fdat);

    /* On enregistre dans itd */
    itd.carte = carte;
    itd.chemin = chemin;
    itd.color_node = color_node;
    itd.construct = construct;
    itd.in = in;
    itd.out = out;
    itd.nbNodes = nbNode;
    itd.nodes = nodes;
    return itd;    
}