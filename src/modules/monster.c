#include <stdio.h>
#include <stdlib.h>
#include "../include/modules/monster.h"
#include "../lib/GM/include/geometry/Vector3D.h"
#include "../lib/GM/include/geometry/Point3D.h"
#include "../lib/GM/include/geometry/geometry.h"
#include "../lib/GM/include/geometry/shapeGL.h"

Monster* createMonster(){
	Monster *m = (Monster *)malloc(sizeof(Monster));
	m->texture = NULL;
	m->width = 0;
	m->height = 0;
	m->life = 0;
	m->lifeT = 0;
    m->resistRocket = 0;
    m->resistLaser = 0;
    m->resistHybrid = 0;
    m->resistMachineGun = 0;
	m->type = BABY;
	m->earn = 0;
	m->speed = 0;
	m->pos.x = 0;
	m->pos.y = 0;
	m->node = 0;
	m->select = 0;
	m->sprite.texture = NULL;
	m->sprite.state = 0;
	m->sprite.width = 0;
	m->sprite.height = 0;
	m->sprite.pos.x = 0;
	m->sprite.pos.y = 0;
	m->sprite.anim = 0;
	m->sprite.timeAnim = 0;
	m->sprite.nbAnim = 0;
	m->sprite.nbState = 0;

	return m;
}

Monster* initMonster(Monster *m, GLuint *textureMonster, Type t, CoordOpenGL in, unsigned int WINDOW_WIDTH, unsigned int WINDOW_HEIGHT, int nbA, int nbS){
	m->texture = textureMonster;
	m->width = 0;
	m->height = 0;
	m->life = 1;
	m->lifeT = 1;
    m->resistRocket = 1;
    m->resistLaser = 1;
    m->resistHybrid = 1;
    m->resistMachineGun = 1;
	m->type = t;
	m->earn = 1;
	m->speed = 0.001;
	m->pos = in;
	m->node = 0;
	m->select = 0;
	switch(t){
		case BABY:
			m->width = SDLtoOpenGL(32, WINDOW_WIDTH, ABSCISSA);
			m->height = SDLtoOpenGL(21, WINDOW_HEIGHT, ORDERED);
			m->life *= 5;
			m->lifeT *= 5;
		    m->resistRocket *= 2;
		    m->resistLaser *= 1;
		    m->resistHybrid *= 1;
		    m->resistMachineGun *= 1;
			m->earn *= 5;
			m->speed *= 3;
			break;
		case LOW:
			m->width = SDLtoOpenGL(24, WINDOW_WIDTH, ABSCISSA);
			m->height = SDLtoOpenGL(24, WINDOW_HEIGHT, ORDERED);
			m->life *= 10;
			m->lifeT *= 10;
		    m->resistRocket *= 1;
		    m->resistLaser *= 2;
		    m->resistHybrid *= 3;
		    m->resistMachineGun *= 1;
			m->earn *= 10;
			m->speed *= 4;
			break;
		case MEDIUM:
			m->width = SDLtoOpenGL(24, WINDOW_WIDTH, ABSCISSA);
			m->height = SDLtoOpenGL(24, WINDOW_HEIGHT, ORDERED);
			m->life *= 20;
			m->lifeT *= 20;
		    m->resistRocket *= 2;
		    m->resistLaser *= 3;
		    m->resistHybrid *= 1;
		    m->resistMachineGun *= 1;
			m->earn *= 20;
			m->speed *= 4;
			break;
		case HARD:
			m->width = SDLtoOpenGL(24, WINDOW_WIDTH, ABSCISSA);
			m->height = SDLtoOpenGL(24, WINDOW_HEIGHT, ORDERED);
			m->life *= 50;
			m->lifeT *= 50;
		    m->resistRocket *= 3;
		    m->resistLaser *= 5;
		    m->resistHybrid *= 5;
		    m->resistMachineGun *= 1;
			m->earn *= 50;
			m->speed *= 4; //1 px en SDL = 0,004166.. en OpenGL Don c pour respecté le cahier des charges nous ne devons pas dépasser 0,0041 en vitesse de déplacement.
			break;
		default:
			fprintf(stderr, "Ce type n'est pas correct.\n");
			return m;
	}
	m->sprite.texture = m->texture;
	m->sprite.state = DOWN;
	m->sprite.width = m->width;
	m->sprite.height = m->height;
	m->sprite.pos = m->pos;
	m->sprite.anim = 0;
	m->sprite.timeAnim = 1;
	m->sprite.nbAnim = nbA;
	m->sprite.nbState = nbS;

	return m;
}

int waveMonster(Player *player, Map *map, List *listMonster, GLuint *textureMonster){
	if(player == NULL || map == NULL || textureMonster == NULL){
		printf("Un des pointeurs est NULL\n");
		return -1;
	}

	if(map->timerWave == 0){
		if(player->lvl > 0){
			if(map->wave < 10){
				Monster *monster;
				monster = createMonster();

				switch(player->lvl){
					case 1:
						monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);	// Créer un monstre.
						setLifeMonster(monster, (player->lvl*4));														// Modifie sa vie en fonction de la vague (lvl du joueur).
						setLifeTMonster(monster, (player->lvl*4));														// Modifie sa vie en fonction de la vague (lvl du joueur).
						setResistRocketMonster(monster, 1);
						setResistLaserMonster(monster, 2);
						*listMonster = appendList(*listMonster, monster);													// Ajoute le monstre à la liste.
						// printf("vague de niveau 1\n");
						break;
					case 2:
						monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistHybridMonster(monster, 1);
						setResistRocketMonster(monster, 2);
						setResistLaserMonster(monster, 2);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 2\n");
						break;
					case 3:
						monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistHybridMonster(monster, 2);
						setResistRocketMonster(monster, 3);
						setResistLaserMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 3\n");
						break;
					case 4:
						monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistHybridMonster(monster, 2);
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 4);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 4\n");
						break;
					case 5:
						if((rand()%2+1) == 1){
							monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);
							setResistHybridMonster(monster, 2);
							setResistRocketMonster(monster, 4);
							setResistLaserMonster(monster, 4);
							setResistMachineGunMonster(monster, 4);
						}
						else{
							monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
							setResistRocketMonster(monster, 3);
							setResistLaserMonster(monster, 3);
							setResistHybridMonster(monster, 2);
						}
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, (player->lvl));
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 5\n");
						break;
					case 6:
						monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 3);
						setResistLaserMonster(monster, 3);
						setResistHybridMonster(monster, 2);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 6\n");
						break;
					case 7:
						monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 3);
						setResistHybridMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 7\n");
						break;
					case 8:
						monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 4);
						setResistHybridMonster(monster, 4);
						setResistMachineGunMonster(monster, 1);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 8\n");
						break;
					case 9:
						monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 5);
						setResistHybridMonster(monster, 4);
						setResistMachineGunMonster(monster, 1);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 9\n");
						break;
					case 10:
						if((rand()%2+1) == 1){
							monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
						}
						else{
							monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						}
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, player->lvl);
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 6);
						setResistHybridMonster(monster, 5);
						setResistMachineGunMonster(monster, 1);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 10\n");
						break;
					case 11:
						monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 5);
						setResistHybridMonster(monster, 2);
						setResistMachineGunMonster(monster, 1);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 11\n");
						break;
					case 12:
						monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 4);
						setResistLaserMonster(monster, 5);
						setResistHybridMonster(monster, 3);
						setResistMachineGunMonster(monster, 2);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 12\n");
						break;
					case 13:
						monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 5);
						setResistLaserMonster(monster, 5);
						setResistHybridMonster(monster, 3);
						setResistMachineGunMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 13\n");
						break;
					case 14:
						monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 6);
						setResistLaserMonster(monster, 6);
						setResistHybridMonster(monster, 4);
						setResistMachineGunMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 14\n");
						break;
					case 15:
						if((rand()%2+1) == 1){
							monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
						}
						else{
							monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
						}
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, player->lvl);
						setResistRocketMonster(monster, 6);
						setResistLaserMonster(monster, 6);
						setResistHybridMonster(monster, 5);
						setResistMachineGunMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 15\n");
						break;
					case 16:
						monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 5);
						setResistLaserMonster(monster, 5);
						setResistHybridMonster(monster, 2);
						setResistMachineGunMonster(monster, 3);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 16\n");
						break;
					case 17:
						monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 7);
						setResistLaserMonster(monster, 7);
						setResistHybridMonster(monster, 4);
						setResistMachineGunMonster(monster, 4);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 17\n");
						break;
					case 18:
						monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 9);
						setResistLaserMonster(monster, 8);
						setResistHybridMonster(monster, 5);
						setResistMachineGunMonster(monster, 5);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 18\n");
						break;
					case 19:
						monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
						setLifeMonster(monster, (player->lvl*4));
						setLifeTMonster(monster, (player->lvl*4));
						setEarnMonster(monster, ((player->lvl)-1));
						setResistRocketMonster(monster, 12);
						setResistLaserMonster(monster, 10);
						setResistHybridMonster(monster, 7);
						setResistMachineGunMonster(monster, 7);
						*listMonster = appendList(*listMonster, monster);
						// printf("vague de niveau 19\n");
						break;
					case 20: // HAHAHA tu as cru que parce que j'avais changé la texture les monstre étaient plus faible !!!
						switch((rand()%4+1)){ // Hé bhé non, c'est tous des HARD !!! EAT !!!
							case 1:
								monster = initMonster(monster, &(textureMonster[0]), BABY, map->nodesGL[0], 960, 720, 3, 4);
								setLifeMonster(monster, (player->lvl*7));
								setLifeTMonster(monster, (player->lvl*7));
								setEarnMonster(monster, player->lvl);
								setResistRocketMonster(monster, 20);
								setResistLaserMonster(monster, 12);
								setResistHybridMonster(monster, 10);
								setResistMachineGunMonster(monster, 10);
								break;
							case 2:
								monster = initMonster(monster, &(textureMonster[1]), LOW, map->nodesGL[0], 960, 720, 3, 4);
								setLifeMonster(monster, (player->lvl*6));
								setLifeTMonster(monster, (player->lvl*6));
								setEarnMonster(monster, player->lvl);
								setResistRocketMonster(monster, 20);
								setResistLaserMonster(monster, 12);
								setResistHybridMonster(monster, 10);
								setResistMachineGunMonster(monster, 10);
								break;
							case 3:
								monster = initMonster(monster, &(textureMonster[2]), MEDIUM, map->nodesGL[0], 960, 720, 3, 4);
								setLifeMonster(monster, (player->lvl*5));
								setLifeTMonster(monster, (player->lvl*5));
								setEarnMonster(monster, player->lvl);
								setResistRocketMonster(monster, 20);
								setResistLaserMonster(monster, 12);
								setResistHybridMonster(monster, 10);
								setResistMachineGunMonster(monster, 10);
								break;
							case 4:
								monster = initMonster(monster, &(textureMonster[3]), HARD, map->nodesGL[0], 960, 720, 3, 4);
								setLifeMonster(monster, (player->lvl*4));
								setLifeTMonster(monster, (player->lvl*4));
								setEarnMonster(monster, player->lvl);
								setResistRocketMonster(monster, 20);
								setResistLaserMonster(monster, 12);
								setResistHybridMonster(monster, 10);
								setResistMachineGunMonster(monster, 10);
								break;
						}
						*listMonster = appendList(*listMonster, monster);
						break;
					default:
						printf("Tricheur !!\n");
						break;
				}
				map->wave++;
			}
			if(player->lvl >= 20){
				if(winWave(*listMonster) == 1 && map->wave > 0){
					map->wave = 99;
				}
				if(winWave(*listMonster) == 1 && map->wave == 99){
					printf("You win !!\n");
					return 1;
				}
			}
			map->timerWave = map->delayMonster;
			return 0;
		}
		else{
			printf("En attente d'une action permettant de lancer la prochaine vague.\n");
			map->timerWave = map->delayMonster;
			return 0;
		}
	}
	else{
		map->timerWave--;
		return 0;
	}
}

int winWave(List listMonster){
	if(lengthList(listMonster) == 0){
		return 1;
	}
	return 0;
}

int routeWay(Monster *m, int nbNodes, CoordOpenGL *nodes){
	if(m == NULL){
		fprintf(stderr, "(routeWay()) -> Le monstre n'existe pas.\n");
		return -1;
	}
	if(m->pos.x == nodes[nbNodes-1].x && m->pos.y == nodes[nbNodes-1].y){	// Si le monstre n'est pas au noeud de sortie alors
		printf("perdu !\n");
		return 1;
	}
	else{																	// Sinon
		if(m->pos.x == nodes[m->node].x && m->pos.y == nodes[m->node].y){	// Si le monstre est arrivé à son noeud objectif
			m->node++;														// Le monstre passe au prochain noeud objectif
		}
		updatePosMonster(m, m->pos, nodes[m->node]);						// Sinon il continu à avancer vers osn objectif
		return 0;
	}
}

void updatePosMonster(Monster *m, CoordOpenGL from, CoordOpenGL to){
	if(m == NULL){
		fprintf(stderr, "(updatePosMonster()) -> Le monstre n'existe pas.\n");
		return;
	}
	Point3D A, B, newPos;
	Vector3D AB;

	A = PointXYZ(from.x, from.y, 0);	// Point de départ (position actuelle du monstre).
	B = PointXYZ(to.x, to.y, 0);		// Point d'arrivé (position du noeud à atteindre).
	AB = Vector(A, B);					// Création du vecteur allant du point de départ au point d'arrivé.
	AB = Normalize(AB);					// normalisation du vecteur.

	newPos = PointPlusVector(A, MultVector(m->speed, AB));	// Calcul de la nouvelle position du monstre.
	if(AB.x == 0 && AB.y > 0){								// Vérifie si la direction du vecteur est UP.
		if(newPos.x == B.x && newPos.y > B.y){	// Gère le cas où le pas du monstre dépasserait le noeud.
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;				// Affectation de la nouvelle position en x.
			m->pos.y = newPos.y;				// Affectation de la nouvelle position en y.
		}
		updateStateSprite(&(m->sprite), UP);
	}
	else if(AB.x == 0 && AB.y < 0){							// Vérifie si la direction du vecteur est DOWN.
		if(newPos.x == B.x && newPos.y < B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		updateStateSprite(&(m->sprite), DOWN);
	}
	else if(AB.x > 0 && AB.y == 0){							// Vérifie si la direction du vecteur est RIGHT.
		if(newPos.x > B.x && newPos.y == B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		updateStateSprite(&(m->sprite), RIGHT);
	}
	else if(AB.x < 0 && AB.y == 0){							// Vérifie si la direction du vecteur est LEFT.
		if(newPos.x < B.x && newPos.y == B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		updateStateSprite(&(m->sprite), LEFT);
	}
	else if(AB.x > 0 && AB.y > 0){							// Vérifie si la direction du vecteur est UP + RIGHT.
		if(newPos.x > B.x && newPos.y > B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		if(AB.y/AB.x > 1){									// Calcule de la pente pour choisir le sens du sprite.
			updateStateSprite(&(m->sprite), UP);
		}
		else{
			updateStateSprite(&(m->sprite), RIGHT);
		}
	}
	else if(AB.x > 0 && AB.y < 0){							// Vérifie si la direction du vecteur est DOWN + RIGHT.
		if(newPos.x > B.x && newPos.y < B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		if(AB.y/AB.x < -1){								// Calcule de la pente pour choisir le sens du sprite.
			updateStateSprite(&(m->sprite), DOWN);
		}
		else{
			updateStateSprite(&(m->sprite), RIGHT);
		}
	}
	else if(AB.x < 0 && AB.y < 0){							// Vérifie si la direction du vecteur est DOWN + LEFT.
		if(newPos.x < B.x && newPos.y < B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		if(AB.y/AB.x < 1){									// Calcule de la pente pour choisir le sens du sprite.
			updateStateSprite(&(m->sprite), DOWN);
		}
		else{
			updateStateSprite(&(m->sprite), LEFT);
		}
	}
	else if(AB.x < 0 && AB.y > 0){							// Vérifie si la direction du vecteur est UP + LEFT.
		if(newPos.x < B.x && newPos.y > B.y){
			m->pos.x = B.x;
			m->pos.y = B.y;
		}
		else{
			m->pos.x = newPos.x;
			m->pos.y = newPos.y;
		}
		if(AB.y/AB.x > -1){									// Calcule de la pente pour choisir le sens du sprite.
			updateStateSprite(&(m->sprite), UP);
		}
		else{
			updateStateSprite(&(m->sprite), LEFT);
		}
	}
	updatePosSprite(&(m->sprite), m->pos); // Met à jour la position du sprite.
}

int checkMonster(CoordOpenGL clic, Monster *m){
    if(m == NULL){
        fprintf(stderr, "checkMonster::Le monstre n'existe pas.\n");
        return 0;
    }
    float XLeft = m->pos.x-(m->width/2); 	// X a gauche du monstre
    float XRight = m->pos.x+(m->width/2);	// X a droite du monstre
    float YUp = m->pos.y+(m->height/2);	// Y en haut
    float YDown = m->pos.y-(m->height/2);	// Y en bas
    if(clic.x < XRight && clic.x > XLeft && clic.y > YDown && clic.y < YUp){
        return 1;
    }
    return 0;
}

int getLifeMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "setLifeMonster:: m n'existe pas\n");
		return 0;
	}
	return m->life;
}

void setLifeMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "setLifeMonster:: m n'existe pas\n");
		return;
	}
	m->life += v;
}

int getLifeTMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "setLifeTMonster:: m n'existe pas\n");
		return 0;
	}
	return m->lifeT;
}

void setLifeTMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "setLifeTMonster:: m n'existe pas\n");
		return;
	}
	m->lifeT += v;
}

int getResistRocketMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "getResistRocketMonster:: m n'existe pas\n");
		return 0;
	}
	return m->resistRocket;
}

void setResistRocketMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "setResistRocketMonster:: m n'existe pas\n");
		return;
	}
	m->resistRocket += v;
}

int getResistHybridMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "getResistHybridMonster:: m n'existe pas\n");
		return 0;
	}
	return m->resistHybrid;
}

void setResistHybridMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "setResistHybridMonster:: m n'existe pas\n");
		return;
	}
	m->resistHybrid += v;
}

int getResistLaserMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "getResistLaserMonster:: m n'existe pas\n");
		return 0;
	}
	return m->resistLaser;
}

void setResistLaserMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "setResistLaserMonster:: m n'existe pas\n");
		return;
	}
	m->resistLaser += v;
}

int getResistMachineGunMonster(Monster *m){
	if(m == NULL){
		fprintf(stderr, "getResistMachineGunMonster:: m n'existe pas\n");
		return 0;
	}
	return m->resistMachineGun;
}

void setResistMachineGunMonster(Monster *m, int v){
	if(m == NULL){
		fprintf(stderr, "getResistMachineGunMonster:: m n'existe pas\n");
		return;
	}
	m->resistMachineGun += v;
}

int getEarnMonster(Monster *m){
	return m->earn;
}

void setEarnMonster(Monster *m, int v){
	m->earn += v;
}

List dieMonster(List listMonster, Player *player, TwBar **twMonster){
	if(listMonster == NULL){
		fprintf(stderr, "dieMonster:: listMonster n'existe pas\n");
		return NULL;
	}
	foreachList(node, listMonster){
		Monster *m = (Monster *)node->data;
		if(m->life < 0){
			if(*twMonster != NULL && twMonster != NULL && m->select == 1){
				TwDeleteBar(*twMonster);
            	*twMonster = NULL;
			}
			int money = getEarnMonster(m);
			setMoneyPlayer(player, money);
			listMonster = removeFromList(listMonster, m);
		}
    }
    return listMonster;
}

void drawMonster(Monster *m){
	drawSprite(&(m->sprite));
}

void drawLifeMonster(Monster *m){
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
		if(m->life > (float)m->lifeT/2){
	        glColor4f (0.0, 1.0, 0.0, 0.80);
		}
		else if(m->life < (float)m->lifeT/2 && m->life > (float)m->lifeT*3/4){
			glColor4f (1.0, 1.0, 0.0, 0.80);	
		}
		else if(m->life < (((float)m->lifeT)*((float)3/4))){
			glColor4f (1.0, 0.0, 0.0, 0.80);
		}
		glTranslatef(((m->pos.x)-(m->width/2)), ((m->pos.y)+(m->height/2)+m->height/10), 0);
	    glScalef(((m->life*m->width)/m->lifeT), (m->height/10), 0);
	    glTranslatef(0.5, 0.5, 0); // replacement de mon quad avant translate du square (canonique -> unitaire)
	    drawQuad();
    glPopMatrix();
}

void drawMonsterSelect(Monster *m){
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
	    glColor4f (1.0, 0.5, 0.0, 0.35);
	    glTranslatef((m->pos.x), (m->pos.y)-(m->height/3), 0);
	    glScalef((m->width)/2, (m->height)/3, 0);
	    drawCircle();			// cercle full
	    glColor4f (1.0, 0.5, 0.0, 1.0);
	    drawOutlineCircle();	// contour du cercle
    glPopMatrix();
}

void deleteMonster(Monster *m){
	free(m);
	m = NULL;
}