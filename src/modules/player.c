#include "../include/modules/player.h"

Player* createPlayer(){
	Player *p = (Player *)malloc(sizeof(Player));
	p->life = 1;
	p->money = 1;
	p->lvl = 0;
	p->param.w_height = 0;
	p->param.w_width = 0;
	p->mouseSDL.x = 1000;
	p->mouseSDL.y = 1000;
	p->constructMode = 0;
	p->gameTimer = 0;

	return p;
}

void initPlayer(Player *p){
	if(p == NULL){
		fprintf(stderr, "Le pointeur de player n'existe pas\n");
		return;
	}
	p->life = 1;
	p->money = 500;
	p->lvl = 0;
	p->param.w_height = 720;
	p->param.w_width = 960;
	p->gameTimer = 600;
}

int getLifePlayer(Player *p){
	if(p == NULL){
		fprintf(stderr, "getLifePlayer::Erreur le player n'existe pas.\n");
	}
	return p->life;
}

void setLifePlayer(Player *p, int v){
	if(p == NULL){
		fprintf(stderr, "setLifePlayer::Erreur le player n'existe pas.\n");
	}
	p->life += v;
}

int getMoneyPlayer(Player *p){
	if(p == NULL){
		fprintf(stderr, "getMoneyPlayer::Erreur le player n'existe pas.\n");
	}
	return p->money;
}

void setMoneyPlayer(Player *p, int v){
	if(p == NULL){
		fprintf(stderr, "setMoneyPlayer::Erreur le player n'existe pas.\n");
	}
	p->money += v;
}

int getLvlPlayer(Player *p){
	if(p == NULL){
		fprintf(stderr, "getLvlPlayer::Erreur le player n'existe pas.\n");
	}
	return p->lvl;
}

void setLvlPlayer(Player *p, int v){
	if(p == NULL){
		fprintf(stderr, "setLvlPlayer::Erreur le player n'existe pas.\n");
	}
	p->lvl += v;
}

void changeConstructMode(Player *p){
	if(p == NULL) return;
	if(p->constructMode == 0){
		p->constructMode = 1;
	}
	else{
		p->constructMode = 0;
	}
}