#include "../include/modules/square.h"
#include <stdio.h>
#include <stdlib.h>

#define EPSILON 0.00001

Square createSquare(float xFirstFrag, float yFirstFrag, int nbFragSquare, float lenghtSquare){ //appelé en boucle
	Square square;
	square.coordGL.x = xFirstFrag;
	square.coordGL.y = yFirstFrag;
	square.nbFragSquare = nbFragSquare;
	square.lenghtSquare = lenghtSquare;
	square.nbFragConstruct = 0;
	square.availability = 0;
	square.initSquare = 0;	// permet de ne pas dessiner le carré tant que notre souris n'a pas de coordonnées à dessiner

	return square;
}

void fulfillSquareConstruct(Square *square, int nbSquare){
	if(square == NULL){
		fprintf(stderr, "Le tableau des carrés n'existe pas\n");
		return;
	}
	int i;
	
	/* On parcourt tous les Fragments du carré en question */
	for(i=0; i<nbSquare; i++){ // de i= y*largeur coté ; i<largeur coté ; i++
		if((square[i].nbFragConstruct*((square->lenghtSquare/square->nbFragSquare)*(square->lenghtSquare/square->nbFragSquare))) > (3*(square->lenghtSquare*square->lenghtSquare)/4)){	// si la moitié des pixels du carré sont constructibles
			square[i].availability = 1;
		}
	}
}

Square findSquare(CoordOpenGL coord, Square *squareTab, int nbSquare){
	if(squareTab == NULL){
		fprintf(stderr, "le tableau de carré(Square) n'existe pas.\n");
		return *squareTab;
	}
	CoordSquare coordTempInt;
	coord.x += 2.0; // Permet de retourné dans un répère exclusivement positif pour faire des comparaisons.
	coord.y += 1.5;
	coordTempInt.x = (int)(coord.x/squareTab->lenghtSquare); // nb de carrés dont on s'est deplacé pour etre au point cliqué
	coordTempInt.y = (int)(coord.y/squareTab->lenghtSquare); // Ne pas oublier d'arrondir ces valeurs float pour caster en int.
	coord.x = coordTempInt.x * squareTab->lenghtSquare; //position du carré
	coord.y = coordTempInt.y * squareTab->lenghtSquare;
	int i = 0;
	while(i<nbSquare){
		if((coord.x - ((squareTab[i].coordGL.x)+2.0))<EPSILON && (coord.y - ((squareTab[i].coordGL.y)+1.5))<EPSILON){
			return squareTab[i];
		}
		i++;
	}
	printf("Erreur : le Square (carré) n'a pas été trouvé\n");
	return *(squareTab);
}

void drawSquare(Square square){
	// printf("DRAW CARRE %f %f\n",square.coordGL.x, (square.coordGL.y) );
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
	    if(square.availability == 0){
	        glColor4f (1.0, 0.0, 0.0, 0.35);
	        glTranslatef((square.coordGL.x), (square.coordGL.y), 0);
	    }
	    else{
	        glColor4f (0.0, 1.0, 0.0, 0.35);
	        glTranslatef((square.coordGL.x), (square.coordGL.y), 0);
	    }
	    glScalef(square.lenghtSquare, square.lenghtSquare, 0);
	    glTranslatef(0.5, 0.5, 0); // replacement de mon quad avant translate du square (canonique -> unitaire)
	    drawQuad();
	    if(square.availability == 0){
	        glColor4f (1.0, 0.0, 0.0, 1.0);
	    }
	    else{
	        glColor4f (0.0, 1.0, 0.0, 1.0);
	    }
	    drawOutlineQuad();
    glPopMatrix();
}

void drawSquareGrid(Square square){
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
        glColor4f (1.0, 1.0, 1.0, 0.15);
        glTranslatef((square.coordGL.x), (square.coordGL.y), 0);
	    glScalef(square.lenghtSquare, square.lenghtSquare, 0);
	    glTranslatef(0.5, 0.5, 0); // replacement de mon quad avant translate du square (canonique -> unitaire)
	    drawOutlineQuad(); // dessine un carré canonique vide (juste les contours)
    glPopMatrix();
}