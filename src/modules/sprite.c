#include <stdio.h>
#include <stdlib.h>
#include "../include/modules/sprite.h"

Sprite createSprite(Sprite s){
	s.texture = NULL;
	s.state = 0;
	s.width = 0;
	s.height = 0;
	s.pos.x = 0;
	s.pos.y = 0;
	s.anim = 0;
	s.timeAnim = 0;
	s.nbAnim = 1; // 1 au minimum évite les division par 0 plus tard.
	s.nbState = 1; // 1 au minimum évite les division par 0 plus tard.

	return s;
}

Sprite initSprite(Sprite s, GLuint *texture, float w, float h, CoordOpenGL pos, int nbA, int nbS){
	s.texture = texture;
	s.state = DOWN;
	s.width = w;
	s.height = h;
	s.pos = pos;
	s.anim = 0;
	s.timeAnim = 1;
	s.nbAnim = nbA;
	s.nbState = nbS;

	return s;
}

void updateStateSprite(Sprite *sprite, State state){ // Vous devez placer cette fonction au bonne endroit dans votre objet qui contiendra un sprite.
	if(sprite->timeAnim <= 0){
		if(sprite->anim == sprite->nbAnim){
			if(sprite->state == DIE){
				return;
			}
			sprite->anim = 0;
		}
		sprite->anim++;
		sprite->timeAnim = 1;	//Temps entre chaque animation.
	}
	else{
		sprite->timeAnim--;
	}
	switch(state){
		case DOWN:
			sprite->state = DOWN;
			break;
		case LEFT:
			sprite->state = LEFT;
			break;
		case RIGHT:
			sprite->state = RIGHT;
			break;
		case UP:
			sprite->state = UP;
			break;
		case DIE:
			sprite->state = DIE;
			break;
		default:
			fprintf(stderr, "Problème lors de la mise à jour du sprite.\n");
			return;
			break;
	}
}

void updatePosSprite(Sprite *s, CoordOpenGL p){ // Placer cette fonction à l'endoit ou la position de l'objet change.
	s->pos = p;
}

void drawSprite(Sprite *s){
	if(s == NULL){
		fprintf(stderr, "drawSprite::Le sprite n'existe pas.\n");
		return;	
	}
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3ub(255, 255, 255);
    glTranslatef(s->pos.x, s->pos.y, 0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *(s->texture));
        glBegin(GL_QUADS);
            //coord
            glTexCoord2f((float)s->anim/(float)s->nbAnim, (float)s->state/(float)s->nbState);
            glVertex2f(-(s->width)/2, (s->height)/2); // divise par 2 pour réduire la taille des monstres sur la map.
            glTexCoord2f((float)s->anim/(float)s->nbAnim+(float)1/(float)s->nbAnim, (float)s->state/(float)s->nbState);
            glVertex2f((s->width)/2, (s->height)/2);
            glTexCoord2f((float)s->anim/(float)s->nbAnim+(float)1/(float)s->nbAnim, (float)s->state/(float)s->nbState+(float)1/(float)s->nbState);
            glVertex2f((s->width)/2, -(s->height)/2);
            glTexCoord2f((float)s->anim/(float)s->nbAnim, (float)s->state/(float)s->nbState+(float)1/(float)s->nbState);
            glVertex2f(-(s->width)/2, -(s->height)/2);
        glEnd();
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_ALPHA_TEST);
}