#include "../include/modules/coordinates.h"

CoordOpenGL coordSDLtoCoordOpenGL(CoordSDL coord, int w_height, int w_width){
	CoordOpenGL coordConvert;
	coordConvert.x = (-2 + 4 * ((float)coord.x/w_width));
	coordConvert.y = (-(-1.5 + 3 * ((float)coord.y/w_height)));
	return coordConvert;
}

CoordSDL coordOpenGLtoCoordSDL(CoordOpenGL coord, int w_height, int w_width){
	CoordSDL coordConvert;
	coordConvert.x = (int)((coord.x +2)*w_width/4);			//attention a faire le cast pour le resultat du calcul final
	coordConvert.y = (int)((-coord.y +1.5)*w_height/3);		//attention a faire le cast pour le resultat du calcul final
	return coordConvert;
}

CoordSDL changeCoordMark(CoordSDL coord, int w_height, int w_width, int w_image, int h_image){
	//printf("%d %d\n",coord.x, coord.y );
	int coeffX,coeffY;
	CoordOpenGL coordTemp;

	coeffX = (3*w_width)/4; // Prendre les 3/4 de SDL pour se retrouver avec la map en largeur.
	coeffY = (w_height);

	coordTemp.x = coord.x* coeffX/w_image;
	coordTemp.y = coord.y* coeffY/h_image;

	if((coordTemp.x + 0.5) > (int)coordTemp.x){
		coordTemp.x += 1.0;
	}
	if((coordTemp.y + 0.5) > (int)coordTemp.y){
		coordTemp.y += 1.0;
	}
	coord.x = (int)coordTemp.x;
	coord.y = (int)coordTemp.y;
	return coord;
}

float SDLtoOpenGL(int x, int scaleSDL, Axes axis){
	float convert;
	if(axis == ABSCISSA){
		convert = (abscissaLength * (float)x/scaleSDL);
	}
	else if(axis == ORDERED){
		convert = (orderedLength * (float)x/scaleSDL);
	}
	return convert;
}

int OpenGLtoSDL(float x, float scaleSDL, Axes axis){
	int convert;
	if(axis == ABSCISSA){
		convert = (int)(x) * scaleSDL/abscissaLength;
	}
	else if(axis == ORDERED){
		convert = (int)(x) * scaleSDL/orderedLength;
	}
	return convert;
}

CoordOpenGL coordSDLmaptoCoordOpenGLmap(CoordSDL coord, int w_height, int w_width, float heightGL, float widthGL){
	CoordOpenGL coordConvert;
	coordConvert.x = (-2 + widthGL * (float)coord.x/w_width);
	coordConvert.y = (-(-1.5 + heightGL * (float)coord.y/w_height));
	return coordConvert;
}

CoordSDL coordOpenGLmaptoCoordSDLmap(CoordOpenGL coord, int w_height, int w_width, float heightGL, float widthGL){
	CoordSDL coordConvert;
	coordConvert.x = (int)(coord.x +2)*w_width/widthGL;
	coordConvert.y = (int)(-coord.y +1.5)*w_height/heightGL;
	return coordConvert;
}

CoordOpenGL coordSquaretoCoordOpenGL(CoordSquare coord, int w_height, int w_width){
	CoordOpenGL coordConvert;
	coord.x *= 16;
	coord.y *= 16;
	coordConvert.x = (-2 + 4 * (float)coord.x/w_width);
	coordConvert.y = (-(-1.5 + 3 * (float)coord.y/w_height));
	return coordConvert;
}
