# $(BIN) est la nom du binaire genere
BIN = itd
# FLAG
FLAGS = -Wall -std=c99
# INCLUDES
INC_LIB = ./lib/GM/include
INC_LIB_ATB = ./lib/AntTweakBar/include
INC = ./include/
# LIBRARY
LIBDIR = lib/
# Librairies
LIBS = -litd -lglut -lGL -lGLU -lSDL -lSDL_image -lAntTweakBar
# Compilateur
CC = gcc
# SOURCES
SRC = src/
SRC_FILES = $(shell find $(SRC) -type f -name '*.c')
# OBJETS
OBJ = obj/
OBJ_FILES = $(patsubst $(SRC)%.c, $(OBJ)%.o, $(SRC_FILES))

all: ./bin/$(BIN)

./bin/$(BIN): $(OBJ_FILES)
	@echo "**** Compilation finale ****"
	@mkdir -p $(@D)
	$(CC)  -o ./bin/$(BIN) $(FLAGS) $(OBJ_FILES) -L$(LIBDIR) $(LIBS)

$(OBJ)%.o: $(SRC)%.c
	@mkdir -p $(@D)
	$(CC) -I$(INC) -I$(INC_LIB) -I$(INC_LIB_ATB) $(FLAGS) -c $< -o $@

.PHONY: clean

clean:
	@rm -rf $(OBJ_FILES)
	@rm -f bin/$(BIN)
	@find . -name "*~" -exec rm {} \;
	@echo "**** Kärcher, leader des solutions de nettoyage propose une gamme de nettoyeurs pour professionnels et particuliers pour tous vos travaux de nettoyage. ****"

bigclean:
	rm -f $(OBJ)*.o $(SRC)*~ $(INC)*~
	find . -name "*~" -exec rm {} \;
