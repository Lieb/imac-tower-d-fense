/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright Imac Tour Defense
	@language C
	@header Header de list.c
		La gestion de tous ce qui concerne les liste chainé
	@updated 2013-04-06
*/

#ifndef _ITD__LIST_H___
#define _ITD__LIST_H___

/*!
    @typedef Structure d'une liste chainé.
    @abstract Structure qui définit une liste.
    @field val, valeur contenu dans la liste chainé.
    @field next, champs suivant permet d'accéder a noeud suivant.
*/
typedef struct node{
  void *data;
  struct node *next;
}Node;

typedef Node* List;

#define foreachList(node, list) \
    for(Node *(node) = (list); (node) != NULL; (node) = (node)->next)

#define lastList(node, list) \
    Node *lastList_node = (list);\
    if(lastList_node != NULL){while(lastList_node->next != NULL){lastList_node = lastList_node->next;}}\
    for(Node *(node) = lastList_node; (node) != NULL; (node) = (node)->next)

    

/*! \def createList()
    \brief Permet de créer une liste.
    \param void *data data à insérer.
*/
List createList(void *data);

/*! \def prependList()
    \brief Permet d'ajouter un élment en tête de liste.
    \param List *list liste à laquelle nous ajoutons l'élément.
    \param void *data data à insérer.
*/
List prependList(List old, void *data);

/*! \def appendList()
    \brief Permet d'ajouter un élment en fin de liste.
    \param List *list liste à laquelle nous ajoutons l'élément.
    \param void *data data à insérer.
*/
List appendList(List list, void *data);

/*! \def removeFirstList()
    \brief Permet de free le premier élément de la liste.
        free le premier élément de la liste.
    \param List list liste sur laquelle nous supprimons le premier élément.
*/
List removeFirstList(List list);

/*! \def removeFromList()
    \brief Permet de free n'importe quel element de la liste.
    \param List list liste sur laquelle on cherche un element.
    \param void *data data à supprimer.
*/

List removeFromList(List list, void *data);

/*! \def lengthList()
    \brief Permet de récupérer la taille de la liste.
    \param List *list Liste sur laquelle on récupère la taille.
*/
int lengthList(List list);

/*! \def printList()
    \brief Permet d'afficher les éléments de la liste.
    \param List *list Liste sur laquelle on récupère les éléments.
*/
void printList(List list);

/*! \def freeList()
    \brief Permet de free la liste.
        free chaque élément de la liste.
    \param List *list liste à supprimer.
*/
void freeList(List list);

#endif