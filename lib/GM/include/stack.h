/*!
	\author Angéline Guignard & Adrien Megueddem
	\copyright Imac Tour Defense
	\language C
	\header Header de pile.c
		La gestion de tous ce qui concerne les PILE
	\updated 2013-04-06
 */

#ifndef _ITD__STACK_H___
#define _ITD__STACK_H___

#include "list.h"

/*!
    \typedef Structure d'une Pile
*/
typedef List Stack;

/*!
    stackPush() permet d'empiler en tete de liste dans la Pile
    \param Stack* p, pointeur sur la tete de la Pile
    \param void* data, valeur a inserer dans le noeud de la première cellule
    \result On change l'adresse du header de la liste
*/
void stackPush(Stack *p, void* data);

/*!
    stackPop() permet de dépiler en tete de liste
    \param Stack* p, pointeur sur la tete de la Pile
    \result On retourne la valeur du noeud qu'on vient de supprimer
*/
void* stackPop(Stack *p);

#endif