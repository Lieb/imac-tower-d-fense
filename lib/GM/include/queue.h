/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright Imac Tour Defense
	@language C
	@header Header de file.c
		La gestion de tous ce qui concerne les FILE
	@updated 2013-04-06
 */

#ifndef _ITD__QUEUE_H___
#define _ITD__QUEUE_H___

#include "list.h"

/*!
    @typedef Structure d'une File
    @abstract Structure qui définit une File
    @field first, pointeur Head
    @field last, pointeur Tail
 */

typedef struct queue{
	Node* first;
	Node* last;
}Queue;

/*!
    Enqueue() permet d'enfiler en fin de liste dans la File
    @param f
        Queue* f, pointeur sur la tete de la File
    @param v
        int v, valeur a inserer 
    @result
        On change l'adresse du tail de la liste
 */

void Enqueue(Queue *f, void *data);

/*!
    Dequeue() permet de défiler en tete de liste dans la File
    @param f
        Queue* f, pointeur sur la tete de la File
    @result
        On renvoi la valeur du noeud qu'on a supprimé
 */

void* Dequeue(Queue *f);

#endif