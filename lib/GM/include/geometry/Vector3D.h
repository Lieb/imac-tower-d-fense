/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright TD5 lancé de rayon
	@language C
	@header Header de Vector3D, fichier concernant uniquement les vecteurs
	@updated 2013-03-28
 */

#ifndef LDR_VECTOR_3D___
#define LDR_VECTOR_3D___

/***************
** Structures **
***************/

/*!
    @typedef Structure d'un vecteur 3D
    @abstract Structure qui définit ce qu'est un vecteur 3D
    @field x coordonnée x
    @field y coordonnée y
    @field z coordonnée z
 */

typedef struct vector3d{
  float x;
  float y;
  float z;
}Vector3D;

/****************
** Définitions **
****************/

/*!
	VectorXYZ() crée un vecteur 3D à partir de 3 coordonnées données
	@param x
        	float x on prend en paramètre la coordonnée x du vecteur
	@param y
        	float y on prend en paramètre la coordonnée x du vecteur
	@param z
        	float z on prend en paramètre la coordonnée x du vecteur
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D VectorXYZ(float x, float y, float z);

/*!
	AddVectors() crée un vecteur 3D à partir de l'addition de 2 autres vecteurs donnés
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@param B
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D AddVectors(Vector3D A, Vector3D B);

/*!
	SubVectors() crée un vecteur 3D à partir de la soustraction de 2 autres vecteurs donnés
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@param B
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D SubVectors(Vector3D A, Vector3D B);

/*!
	VectorProduct() crée un vecteur 3D à partir du produit vectoriel de 2 autres vecteurs donnés
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@param B
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D VectorProduct(Vector3D A, Vector3D B);

/*!
	MultVector() crée un vecteur 3D à partir de la multiplication d'un scalaire à un vecteurs donnés
	@param s
        	float s on prend en paramètre un scalaire s
	@param A
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D MultVector(float s, Vector3D A);

/*!
	DivVector() crée un vecteur 3D à partir de la division d'un scalaire à un vecteurs donnés
	@param s
        	float s on prend en paramètre un scalaire s
	@param A
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D DivVector(float s, Vector3D A);

/*!
	DotProduct() calcule le produit scalaire de 2 vecteurs
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@param B
        	Vector3D B on prend en paramètre un vecteur B
	@result
		on retourne le résultat du produit scalaire des 2 vecteurs passés en paramètre.
 */

float DotProduct(Vector3D A, Vector3D B);

/*!
	Norm() calcule la norm d'un vecteur
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@result
		on retourne la norm du vecteur passé en paramètre.
 */

float Norm(Vector3D A);

/*!
	Normalize() créer la norme d'un vecteur
	@param A
        	Vector3D A on prend en paramètre un vecteur A
	@result
		on retourne le vecteur normale du vecteur passé en paramètre.
 */

Vector3D Normalize(Vector3D A);

#endif
