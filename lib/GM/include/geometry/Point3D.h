/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright TD5 lancé de rayon
	@language C
	@header Header de Point3D, fichier concernant uniquement les points
	@updated 2013-03-28
 */

#ifndef LDR_POINT_3D___
#define LDR_POINT_3D___

/***************
** Structures **
***************/

/*!
    @typedef Structure d'un point 3D
    @abstract Structure qui définit ce qu'est un point 3D
    @field x coordonnée x
    @field y coordonnée y
    @field z coordonnée z
 */

typedef struct point3d{
  float x;
  float y;
  float z;
}Point3D;

/****************
** Définitions **
****************/

/*!
	PointXYZ() crée un point 3D à partir de 3 coordonnées données
	@param x
        	float x on prend en paramètre la coordonnée x du point
	@param y
        	float y on prend en paramètre la coordonnée x du point
	@param z
        	float z on prend en paramètre la coordonnée x du point
	@result
		on retourne le nouveau point 3D crée
 */

Point3D PointXYZ(float x, float y, float z);

#endif
