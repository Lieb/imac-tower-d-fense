/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright TD5 lancé de rayon
	@language C
	@header Header de geometry, calcul entre point3D et vecteur3D
	@updated 2013-03-28
 */

#ifndef LDR_GEOMETRY___
#define LDR_GEOMETRY___

#include "geometry/Point3D.h"
#include "geometry/Vector3D.h"

/****************
** Définitions **
****************/

/*!
	Vector() crée un vecteur 3D à partir de 2 points 3D donnés
	@param A
        	Point3D A on prend en paramètre un point A
	@param B
        	Point3D B on prend en paramètre un point B
	@result
		on retourne le nouveau vecteur 3D crée
 */

Vector3D Vector(Point3D A, Point3D B);

/*!
	PointPlusVector() additionne un Point3D avec un Vector3D
	@param P
        	Point3D P on prend en paramètre un point P
	@param V
        	Vector3D V on prend en paramètre un vecteur V
	@result
		on retourne un nouveau point3D
 */

Point3D PointPlusVector(Point3D P, Vector3D V);

#endif
