/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright Imac Tour Defense
	@language C
	@header Header de shapeGL.c
		La gestion de tous ce qui concerne les formes (canonique) de bases en OpenGL
	@updated 2013-04-06
 */

#ifndef _ITD__SHAPEGL_H___
#define _ITD__SHAPEGL_H___

#include <GL/gl.h>
#include <GL/glu.h>

/*! \def drawQuad()
    \brief Permet de dessiner un carré canonique
    \result dessine un carré de coté 1 et centré à l'origine du repere orthogonal
*/
void drawQuad();

void drawOutlineQuad();

void drawCircle();

void drawOutlineCircle();

#endif