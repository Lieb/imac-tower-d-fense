/*!
	@author Angéline Guignard & Adrien Megueddem
	@copyright Imac Tour Defense
	@language C
	@header Header de ppm.c
		La gestion de tous ce qui concerne les fichiers ppm
	@updated 2013-04-06
 */

#ifndef _ITD__PPM_H___
#define _ITD__PPM_H___

/**************
** Structures **
**************/

/*!
    @typedef Structure d'un pixel RGB
    @abstract Structure qui définit ce qu'est un pixel (composé de R, G et B)
    @field R, couleur rouge du pixel
    @field G, couleur verte du pixel
    @field B, couleur bleue du pixel
 */

typedef struct pixelsRGB{
	unsigned char R;
	unsigned char G;
	unsigned char B;
}PixelsRGB;

/*!
    @typedef Structure d'une image
    @abstract Structure qui définit ce qu'est une image
    @field nbLines, définit le nombre de lignes de l'image qui équivaut à la hauteur.
    @field nbColumns, définit le nombre de colonnes de l'image qui équivaut à la largeur.
    @field maxVal, donne la valeur max de la couleur d'un pixel
    @field pixels, tableau de pixels qui répertori tout les pixels qui servent à construire l'image.
 */
 
typedef struct image{
    unsigned char* type;
    int nbColumns;
    int nbLines;
    int maxVal;
    PixelsRGB* pixels;
}Image;

/**************
** Définitions **
**************/

/*!
    readppm() permet de lire le fichier ppm et le stock dans une structure Image
    @param nom
        char* nom, chemin d'accès ou nom du fichier ppm que l'on veut cibler
    @result
        renvoie les données du fichier ppm dans une variable de type Image
 */

Image* readppm(char* nom);

/*!
    writeppm() recupere les donnees contenues dans une structure Image et ecrit le fichier .ppm correspondant
    @param image
        Image* image données de l'image
    @param nom
        char* nom chemin d'accès du fichier dans lequel on va écrire
    @result
        renvoie les données du fichier ppm dans une variable de type Image
 */

int writeppm(Image* image, char* nom);

#endif
