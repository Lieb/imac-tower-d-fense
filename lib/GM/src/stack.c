#include "../include/stack.h"
#include <stdlib.h>
#include <stdio.h>

void stackPush(Stack *p, void* data){
	Node* n = (Node*) malloc(sizeof(Node));
	if(n == NULL){
		fprintf(stderr, "Erreur d'allocation mémoire\n");
		return;
	}
	else{
		n->data = data;
		n->next = *p;
		(*p) = n; // On décale la tete de liste
	}
}

void* stackPop(Stack *p){
	if((*p) == NULL){
		fprintf(stderr, "Erreur la pile n'existe pas\n");
		return NULL;
	}
	Node* n = (*p); // On met l'adresse de la liste dans n
	void* data = n->data; // On sauvegarde la valeur du premier noeud
	(*p) = n->next; // On deplace la tete de liste au noeud suivant
	free(n); // On free n et donc l'ancien premier noeud
	return data;
}