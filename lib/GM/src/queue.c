#include "../include/queue.h"
#include <stdlib.h>
#include <stdio.h>

void Enqueue(Queue *f, void *data){
	Node *n = (Node*)malloc(sizeof(Node));
	if(n == NULL){
		fprintf(stderr, "Erreur d'allocation mémoire\n");
		return;
	}
	n->data = data;
	n->next = NULL; // On ajoute en fin de liste
	if(f->last != NULL){ // File non vide
		f->last->next = n; // On rattache notre noeud au dernier noeud de la File
	}
	f->last = n; // On décale le pointeur de Tail sur le nouveau noeud
	if(f->first == NULL){ // Si la File n'existait pas avant, le pointeur Tail est aussi Head
		f->first = f->last;
	}
}

void* Dequeue(Queue *f){
	if(f == NULL){
		return NULL;
	}
	if(f->first == NULL){ // La file existe mais est vide
		return NULL;
	}
	Node *n = f->first; // On recupere la tete la liste
	void *data = n->data;
	f->first = (f->first)->next; // On decale Head vers le noeud d'après
	free(n); // On free en début de liste
	return data;
}