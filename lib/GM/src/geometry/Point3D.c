#include "geometry/Point3D.h"
#include <stdio.h>
#include <stdlib.h>

Point3D PointXYZ(float x, float y, float z){
  Point3D point;
  point.x = x;
  point.y = y;
  point.z = z;  
  return point;
}
