#include "../include/geometry/shapeGL.h"
#include "math.h"

void drawQuad(){
  glBegin(GL_QUADS);
  glVertex2f(-0.5, 0.5);
  glVertex2f(0.5, 0.5);
  glVertex2f(0.5, -0.5);
  glVertex2f(-0.5, -0.5);
  glEnd();
}

void drawOutlineQuad(){
  glBegin(GL_LINE_STRIP);
  glVertex2f(-0.5, 0.5);
  glVertex2f(0.5, 0.5);
  glVertex2f(0.5, -0.5);
  glVertex2f(-0.5, -0.5);
  glVertex2f(-0.5, 0.5);
  glEnd();
}

void drawCircle(){
  float i = 0.00;
  glBegin(GL_TRIANGLE_FAN);      
  for(i = 0; i <= (2*M_PI); i += 0.1 ) { 
    glVertex2f(cos(i), sin(i));
  }
  glEnd();
}

void drawOutlineCircle(){
  float i = 0.00;
  glBegin(GL_LINE_STRIP);      
  for(i = 0; i <= (2*M_PI); i += 0.1 ) { 
    glVertex2f(cos(i), sin(i));
  }
  glEnd();
}

