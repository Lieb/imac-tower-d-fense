#include "geometry/Vector3D.h"
#include "geometry/Point3D.h"
#include "geometry/geometry.h"

Vector3D Vector(Point3D A, Point3D B){
	Vector3D vectorAB;

	vectorAB.x = B.x - A.x;
	vectorAB.y = B.y - A.y;
	vectorAB.z = B.z - A.z;
	return vectorAB;
}

Point3D PointPlusVector(Point3D P, Vector3D V){
	Point3D point;

	point.x = P.x + V.x;
	point.y = P.y + V.y;
	point.z = P.z + V.z;
	return point;
}
