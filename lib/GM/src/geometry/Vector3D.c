#include "geometry/Vector3D.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

Vector3D VectorXYZ(float x, float y, float z){
  Vector3D vector;
  vector.x = x;
  vector.y = y;
  vector.z = z;  
  return vector;
}

Vector3D AddVectors(Vector3D A, Vector3D B){
	Vector3D vectorC;

	vectorC.x = A.x + B.x;
	vectorC.y = A.y + B.y;
	vectorC.z = A.z + B.z;
	return vectorC;
}

Vector3D SubVectors(Vector3D A, Vector3D B){
	Vector3D vectorC;

	vectorC.x = A.x - B.x;
	vectorC.y = A.y - B.y;
	vectorC.z = A.z - B.z;
	return vectorC;
}

Vector3D VectorProduct(Vector3D A, Vector3D B){
	Vector3D vectorC;

	vectorC.x = (A.y)*(B.z) - (A.z)*(B.y);
	vectorC.y = (A.z)*(B.x) - (A.x)*(B.z);
	vectorC.z = (A.x)*(B.y) - (A.y)*(B.x);
	return vectorC;
}

Vector3D MultVector(float s, Vector3D A){
	Vector3D vectorC;

	vectorC.x = s*A.x;
	vectorC.y = s*A.y;
	vectorC.z = s*A.z;
	return vectorC;
}

Vector3D DivVector(float s, Vector3D A){
	Vector3D vectorC;

	vectorC.x = A.x/s;
	vectorC.y = A.y/s;
	vectorC.z = A.z/s;
	return vectorC;
}

float DotProduct(Vector3D A, Vector3D B){
	return A.x * B.x + A.y * B.y + A.z * B.z;
}

float Norm(Vector3D A){	
	return sqrt(pow(A.x, 2)+pow(A.y, 2)+pow(A.z, 2));
}

Vector3D Normalize(Vector3D A){
	Vector3D vectorU;
	float nA;
	
	nA = Norm(A);
	vectorU.x = A.x/nA;
	vectorU.y = A.y/nA;
  	vectorU.z = A.z/nA;
	return vectorU;
}
