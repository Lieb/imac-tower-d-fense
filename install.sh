#!/bin/bash

echo "=========== FIXING ENV VARIABLE =================="
#if [ "1$LD_LIBRARY_PATH" = "1" ]
#then
DIR_LIB=`pwd`
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR_LIB/lib/:$DIR_LIB/lib/GM/include/:$DIR_LIB/lib/AntTweakBar/include/
export LD_LIBRARY_PATH
#fi
echo $LD_LIBRARY_PATH

echo "============== COMPILING LIB =================="
cd ./lib/GM
make
cd ../AntTweakBar/src
make
cd ../../..

echo "============== COMPILING ITD =================="
make
